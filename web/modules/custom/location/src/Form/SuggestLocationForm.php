<?php

namespace Drupal\location\Form;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Location Suggestion form.
 */
class SuggestLocationForm extends FormBase {

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected CountryRepositoryInterface $countryRepository;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The Mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The available values of the requester's connection to the location.
   *
   * @var array
   */
  protected array $locationConnectionOptions = [];

  /**
   * Constructs the SuggestLocationForm.
   */
  public function __construct() {
    $this->locationConnectionOptions = [
      0 => $this->t('I own this Location'),
      1 => $this->t("I'm working at this Location"),
      2 => $this->t('I only know this place because I visited it before'),
      3 => $this->t('Other'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->countryRepository = $container->get('address.country_repository');
    $instance->currentUser = $container->get('current_user');
    $instance->mailManager = $container->get('plugin.manager.mail');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'location_suggest_location';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['location'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Location information'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $form['location']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('The name of the location.'),
      '#required' => TRUE,
    ];

    $form['location']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The description of the location.'),
      '#required' => TRUE,
    ];

    $form['location']['address'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Address'),
      '#description' => $this->t('The address information of the location'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $countries = $this->countryRepository->getList();
    $form['location']['address']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#description' => $this->t('The country of the location.'),
      '#options' => $countries,
      '#required' => TRUE,
    ];

    $form['location']['address']['address_line'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address line'),
      '#description' => $this->t('The address line of the location.'),
      '#required' => TRUE,
    ];

    $form['location']['address']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t('The city of the location.'),
      '#required' => TRUE,
    ];

    $form['location']['address']['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#description' => $this->t('The postal code of the location.'),
      '#required' => TRUE,
    ];

    $form['location']['contact_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Contact email'),
      '#description' => $this->t('The contact email of the location.'),
      '#required' => TRUE,
    ];

    $form['location']['website'] = [
      '#type' => 'url',
      '#title' => $this->t('Website'),
      '#description' => $this->t('The website of the location.'),
    ];

    $form['location']['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone number'),
      '#description' => $this->t('The phone number of the location.'),
    ];

    $form['location']['equipment'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Equipment'),
      '#description' => $this->t('The equipment this location provides.'),
      '#options' => $this->getEquipmentOptions(),
    ];

    $form['requester'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Your information'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $form['requester']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#required' => TRUE,
    ];

    $form['requester']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email'),
      '#required' => TRUE,
      '#default_value' => $this->currentUser->getEmail(),
    ];

    $form['requester']['connection'] = [
      '#type' => 'radios',
      '#title' => $this->t('Your connection to the location'),
      '#options' => $this->locationConnectionOptions,
    ];

    $form['requester']['connection_other'] = [
      '#type' => 'textfield',
      '#states' => [
        'visible' => [
          ':input[name="requester[connection]"]' => [
            'value' => 3,
          ],
        ],
        'required' => [
          ':input[name="requester[connection]"]' => [
            'value' => 3,
          ],
        ],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Suggest'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $location = $this->createLocationNode($form_state);
    if ($location === NULL) {
      \Drupal::messenger()->addError($this->t('Your location suggestion failed.'));

      return;
    }
    if (!$this->sendLocationSuggestionEmail($location, $form_state)) {
      \Drupal::messenger()->addMessage($this->t('Your location suggestion was created successfully!'));

      return;
    }

    \Drupal::messenger()->addMessage($this->t('Your location suggestion was sent successfully!'));
  }

  /**
   * Provides the options from the equipment vocabulary.
   *
   * @return array
   *   The array of options in the form of id => name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEquipmentOptions(): array {
    $equipmentTerms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'equipment',
      ]);

    return array_map(function (Term $equipmentTerm) {
      return $equipmentTerm->getName();
    }, $equipmentTerms);
  }

  /**
   * Creates a Location node from the provided form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return \Drupal\node\Entity\Node|null
   *   The created Location node or NULL if unsuccessfull.
   */
  protected function createLocationNode(FormStateInterface $formState): Node|NULL {
    $location = Node::create([
      'type' => 'location',
      'status' => 0,
      'field_created_by_suggestion' => TRUE,
      'title' => $formState->getValue('location')['name'],
      'field_title' => $formState->getValue('location')['name'],
      'field_description' => $formState->getValue('location')['description'],
      'field_contact_email' => $formState->getValue('location')['contact_email'],
      'field_address' => [
        0 => [
          'country_code' => $formState->getValue('location')['address']['country'],
          'address_line1' => $formState->getValue('location')['address']['address_line'],
          'locality' => $formState->getValue('location')['address']['city'],
          'postal_code' => $formState->getValue('location')['address']['postal_code'],
        ],
      ],
    ]);

    if (!empty($formState->getValue('location')['website'])) {
      $location->set('field_website', $formState->getValue('location')['website']);
    }

    if (!empty($formState->getValue('location')['phone_number'])) {
      $location->set('field_phone_number', $formState->getValue('location')['phone_number']);
    }

    $selectedEquipment = array_filter($formState->getValue('location')['equipment']);
    if (!empty($selectedEquipment)) {
      $location->set('field_equipment', $selectedEquipment);
    }

    try {
      $location->save();

      return $location;
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('location')->error($e->getMessage());

      return NULL;
    }
  }

  /**
   * Sends the location suggestion email to suggestions admins.
   *
   * @param \Drupal\node\NodeInterface $location
   *   The Location node that was created from the suggestion.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state of the suggestion.
   *
   * @return bool
   *   Indicating whether the email was successfully accepted PHP-side.
   */
  protected function sendLocationSuggestionEmail(NodeInterface $location, FormStateInterface $formState): bool {
    $connectionFormValue = (int) $formState->getValue('requester')['connection'];
    $requesterConnection = match(TRUE) {
      $connectionFormValue === 3 => $formState->getValue('requester')['connection_other'],
      default => $this->locationConnectionOptions[$connectionFormValue],
    };

    $to = implode(', ', $this->getSuggestionsAdminEmails());
    $params = [
      'location_data' => $location,
      'requester_data' => [
        'name' => $formState->getValue('requester')['name'],
        'email' => $formState->getValue('requester')['email'],
        'connection' => $requesterConnection,
      ],
    ];

    $response = $this->mailManager->mail('location', 'location_suggestion_email', $to, 'en', $params, NULL, TRUE);

    return $response['result'];
  }

  /**
   * Queries for and returns emails of users with the suggestions admin role.
   *
   * @return array
   *   The array of emails.
   */
  protected function getSuggestionsAdminEmails(): array {
    $suggestionsAdminIds = \Drupal::entityQuery('user')
      ->condition('roles', 'suggestions_admin')
      ->execute();

    $suggestionsAdmins = User::loadMultiple($suggestionsAdminIds);
    $suggestionsAdminEmails = [];
    foreach ($suggestionsAdmins as $suggestionsAdmin) {
      $suggestionsAdminEmails[] = $suggestionsAdmin->getEmail();
    }
    return array_filter($suggestionsAdminEmails);
  }

}

<?php

namespace Drupal\location\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the location's rating to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "add_location_rating",
 *   label = @Translation("Location Rating"),
 *   description = @Translation("Adds the location's rating to the indexed data."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class AddRating extends ProcessorPluginBase {

  const PROPERTY_PATH = 'location_rating';

  /**
   * {@inheritDoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Rating'),
        'description' => $this->t('The rating of the location'),
        'type' => 'integer',
        'processor_id' => $this->getPluginId(),
      ];

      $properties[self::PROPERTY_PATH] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function addFieldValues(ItemInterface $item) {
    $datasourceId = $item->getDatasourceId();

    if ($datasourceId === 'entity:node') {
      $locationId = $item->getOriginalObject()->getValue()->id();

      $rating = \Drupal::service('equipment.rating')
        ->getLocationRating($locationId);

      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($item->getFields(), NULL, self::PROPERTY_PATH);

      foreach ($fields as $field) {
        $field->addValue($this->getRatingText($rating));
      }
    }
  }

  /**
   * Returns the text for the specified rating.
   */
  private function getRatingText($rating) {
    if ($rating === 1) {
      return '1 star';
    }

    return $rating . ' stars';
  }

}

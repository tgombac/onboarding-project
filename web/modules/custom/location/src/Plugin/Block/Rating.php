<?php

namespace Drupal\location\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block that displays Location rating.
 *
 * @Block(
 *  id = "location_rating_block",
 *  admin_label = @Translation("Location Rating Block"),
 *  category = @Translation("Location")
 * )
 */
class Rating extends BlockBase {

  /**
   * {@inheritDoc}
   *
   * @throws \Exception
   */
  public function build(): array {
    $node = \Drupal::routeMatch()->getParameter('node');

    if ($node->getType() !== 'location') {
      throw new \Exception('Node is not of ContentType location');
    }

    $equipmentRatingService = \Drupal::service('equipment.rating');

    $rating = $equipmentRatingService->getLocationRating($node->id());

    return [
      '#theme' => 'location_rating_block',
      '#rating' => $rating,
    ];
  }

  /**
   * Set the cache tags to invalidate cache when any location node changes.
   */
  public function getCacheTags() {
    return ['node_list:location'];
  }

  /**
   * Set the cache context to vary by url, to cache specific location nodes.
   */
  public function getCacheContexts() {
    return ['url'];
  }

}

<?php

use Drupal\node\Entity\Node;

/**
 * Implements hook_post_update_NAME().
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function location_post_update_after_adding_field_is_operating_fill_values() {
  $nids = \Drupal::entityQuery('node')->condition('type', 'location')->execute();

  $events = Node::loadMultiple($nids);

  foreach ($events as $event) {
    $event->set('field_is_operating', TRUE);

    $event->save();
  }
}

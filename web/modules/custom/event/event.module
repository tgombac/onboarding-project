<?php

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\event\Service\EventService;
use Drupal\group\Entity\Group;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\user\Entity\User;

/**
 * Implements hook_theme().
 */
function event_theme(): array {
  return [
    'event_info_block' => [
      'variables' => [
        'content' => '',
        'content_attributes' => [],
      ],
    ],
    'event_favorites_block' => [
      'variables' => [
        'count' => '',
        'link' => '',
      ],
    ],
  ];
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_event_info_block(&$variables) {
  // Convert attributes to a proper \Drupal\Core\Template\Attribute object.
  $variables['content_attributes'] = new Attribute($variables['content_attributes']);
}

/**
 * Implements hook_mail().
 */
function event_mail($key, &$message, $params) {
  // Each message is associated with a language, which may or may not be the
  // current user's selected language, depending on the type of e-mail being
  // sent. This $options array is used later in the t() calls for subject
  // and body to ensure the proper translation takes effect.
  $options = [
    'langcode' => $message['langcode'],
  ];

  // Send event info invitation email from send to friend form.
  if ($key === 'event_info_mail') {
    $message['subject'] = t('@name invited you to @event_title', [
      '@name' => $params['form_values']['name'],
      '@event_title' => $params['event_values']['title'],
    ], $options);

    $message['body'][] = t("Hi! Check out this event: @event_title. It's going to take place at @location_title on @event_date at @event_time UTC.", [
      '@event_title' => $params['event_values']['title'],
      '@location_title' => $params['event_values']['location_title'],
      '@event_date' => $params['event_values']['event_start_date'],
      '@event_time' => $params['event_values']['event_start_time'],
    ]);

    $message['body'][] = t("Read more: @event_link", [
      '@event_link' => $params['event_values']['link'],
    ]);
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function ad_general_form_node_event_edit_form_alter(&$form, $form_state, $form_id) {
  $eventNode = $form_state->getFormObject()->getEntity();

  $endDateISO = $eventNode->get('field_end_date')->getString();

  // Match timezones for accurate comparison.
  $endDateUTC     = new DrupalDateTime($endDateISO, 'UTC');
  $currentDateUTC = new DrupalDateTime('now', 'UTC');

  // Don't show gallery field to editors if the event has not yet ended.
  if ($endDateUTC > $currentDateUTC) {
    $form['field_gallery']['#access'] = FALSE;
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function event_node_presave(EntityInterface $node) {
  if ($node->bundle() === 'event') {
    $title = $node->get('field_title')->getString();

    $eventService = \Drupal::getContainer()->get(EventService::class);

    $titleCaseTitle = $eventService->convertToTitleCase($title);

    $node->set('field_title', $titleCaseTitle);
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function event_node_insert(EntityInterface $node) {
  if ($node->bundle() !== 'event') {
    return;
  }

  $eventService = \Drupal::getContainer()->get(EventService::class);
  try {
    $eventService->generateNotificationMessage($node);
  } catch (EntityStorageException $e) {
    Drupal::messenger()->addError($e->getMessage());
  }

  $item = (object) [
    'nid' => $node->get('field_organizer')->target_id,
  ];
  $queueFactory = \Drupal::service('queue');
  $queueFactory->get('company_news_updater')->createItem($item);
}

/**
 * Implements hook_views_query_alter().
 */
function event_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() === 'event_notifications') {
    $user = User::load(\Drupal::currentUser()->id());
    $maxNumberOfNotifications = $user->get('field_max_number_of_notification')->getString();

    $view->initPager();
    $view->getPager()->setItemsPerPage((int)$maxNumberOfNotifications);
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function event_preprocess_node(&$variables) {
  if ($variables['node']->bundle() === "event" && isset($variables['content']['field_end_date']['#items'])) {
    $eventService = \Drupal::getContainer()->get(EventService::class);

    $eventEndDateUTC = $variables['content']['field_end_date']['#items']->getString();

    if ($eventService->getDaysUntil($eventEndDateUTC) > 0) {
      $variables['content']['field_rating']['#access'] = FALSE;
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function event_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($entity->bundle() !== 'event') {
    return;
  }

  $eventGroupContent = \Drupal::entityTypeManager()
    ->getStorage('group_content')
    ->loadByProperties([
      'entity_id' => $entity->id(),
    ]);
  if (!empty($eventGroupContent)) {
    return;
  }

  $eventGroup = Group::create([
    'type' => 'event_members',
    'label' => $entity->label() . ' members',
  ]);
  $eventGroup->save();

  $pluginId = 'group_' . $entity->getEntityTypeId() . ':' . $entity->bundle();
  $eventGroup->addContent($entity, $pluginId);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function event_preprocess_field__comment(&$variables) {
  $event = \Drupal::routeMatch()->getParameter('node');
  if (empty($event) || $event->bundle() !== 'event') {
    return;
  }

  if (!isset($variables['comment_form'])) {
    return;
  }

  $currentUser = \Drupal::currentUser();

  $eventGroup = \Drupal::service('event.event_group')->getGroup($event);
  $isMember = $eventGroup->getMember($currentUser);
  if ($isMember && $eventGroup->hasPermission('post event comment', $currentUser)) {
    return;
  }

  unset($variables['comment_form']);
}

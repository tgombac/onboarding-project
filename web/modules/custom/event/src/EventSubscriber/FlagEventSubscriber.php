<?php

namespace Drupal\event\EventSubscriber;

use Drupal\Core\Ajax\ReplaceCommand;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Listens for flag / unflag events and updates flag count in favorites block.
 */
class FlagEventSubscriber implements EventSubscriberInterface {

  /**
   * The count wrapper selector.
   *
   * @var string
   */
  private static $selector = '.flag-favorite_event-count-favorites';

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onResponse'];

    return $events;
  }

  /**
   * Returns $selector without the initial dot.
   */
  public static function getSelectorClass() {
    return substr(self::$selector, 1);
  }

  /**
   * Returns a span containing the updated count variable.
   */
  public static function getReplaceSpan($count) {
    return '<span class="' . self::getSelectorClass() . '">' . $count . '</span>';
  }

  /**
   * Return flagging count of favorite_event flag for the current user.
   */
  public static function getFlagCount() {
    $flagCountService = \Drupal::service('flag.count');

    $favoriteEventFlag = \Drupal::service('flag')->getAllFlags()['favorite_event'];

    return $flagCountService->getUserFlagFlaggingCount($favoriteEventFlag, \Drupal::currentUser());
  }

  /**
   * Update flag count in favorites block dynamically.
   */
  public function onResponse(ResponseEvent $event) {
    $request = $event->getRequest();

    // Check if this is a post request to a flag endpoint.
    if ($request->getMethod() === 'POST' && $request->get('flag') !== NULL) {
      $flag = $request->get('flag');

      // Looking for flagging / unflagging of favorite_event.
      if ($flag->id() === 'favorite_event') {
        $response = $event->getResponse();

        $count = self::getFlagCount();

        $response->addCommand(new ReplaceCommand(self::$selector, self::getReplaceSpan($count)));

        $event->setResponse($response);
      }
    }
  }

}

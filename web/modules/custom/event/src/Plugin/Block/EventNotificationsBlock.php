<?php

namespace Drupal\event\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\message\Entity\Message;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Provides an Event Notifications block.
 *
 * @Block(
 *  id = "event_notifications_code_block",
 *  admin_label = @Translation("Event Notifications Block - Code"),
 *  category = @Translation("Event")
 * )
 */
class EventNotificationsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $currentUserId = \Drupal::currentUser()->id();

    $query = \Drupal::entityQuery('message');
    $query->condition('template', 'event_notification', '=');
    $query->condition('field_recipients', $currentUserId, 'IN');
    $query->sort('created', 'DESC');

    $messageIds = $query->execute();

    $messages = Message::loadMultiple($messageIds);

    $user = User::load($currentUserId);
    $maxNumberOfNotifications = $user->get('field_max_number_of_notification')->getString();

    $output = '';
    $i = 0;
    foreach ($messages as $message) {
      if ($i >= $maxNumberOfNotifications) {
        break;
      }

      $eventId = $message->get('field_node_reference')->getString();

      $event = Node::load($eventId);

      $eventStartDateISO = $event->get('field_start_date')->getString();

      $eventStartDate = new DrupalDateTime($eventStartDateISO, 'UTC');
      $currentDate    = new DrupalDateTime('now', 'UTC');

      if ($currentDate <= $eventStartDate) {
        $date = date('j. F Y H:i', $message->getCreatedTime());

        $output .= $date . '<br />' . $message->getText()[0];

        $i++;
      }
    }

    return [
      '#markup' => $output,
    ];
  }

  /**
   * Disable block caching.
   */
  public function getCacheMaxAge(): int {
    return 0;
  }

}

<?php

namespace Drupal\event\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\event\Service\EventService;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Weather Forecast block for the current event's location.
 *
 * @Block(
 *  id = "event_weather_forecast_block",
 *  admin_label = @Translation("Event Weather Forecast Block"),
 *  category = @Translation("Event")
 * )
 */
class WeatherForecast extends BlockBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The Event Service.
   *
   * @var \Drupal\event\Service\EventService
   */
  private EventService $eventService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $eventService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->eventService = $eventService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get(EventService::class)
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function build(): array|NULL {
    $event = \Drupal::routeMatch()->getParameter('node');
    if (!$event) {
      return NULL;
    }
    if ($event->getType() !== 'event') {
      return NULL;
    }

    $daysUntilEvent = $this->eventService->getDaysUntil($event->field_start_date->value);
    if ($daysUntilEvent > 5 || $daysUntilEvent < 0) {
      return NULL;
    }

    return [
      'build' => [
        '#lazy_builder' => [
          static::class . '::weatherInformation', [
            $event->id(),
          ],
        ],
        '#create_placeholder' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['weatherInformation'];
  }

  /**
   * Lazy build the weather information of the event.
   *
   * @param string $eventId
   *   The event id of the event to retrieve the weather forecast for.
   *
   * @return array
   *   The render array.
   *
   * @throws \Exception
   */
  public static function weatherInformation(string $eventId): array {
    $eventService = \Drupal::service(EventService::class);
    $weatherForecastService = \Drupal::service('event.weather.forecast');

    $event = Node::load($eventId);
    $eventStartDate = $event->field_start_date->value;

    $location = $eventService->getEventLocation($event);
    $locationLatitude = (string) $location->field_geofield->lat;
    $locationLongitude = (string) $location->field_geofield->lon;

    $weatherForecast = $weatherForecastService->getWeatherForecast(new \DateTime($eventStartDate), $locationLatitude, $locationLongitude);

    return [
      '#markup' => $weatherForecast,
    ];
  }

}

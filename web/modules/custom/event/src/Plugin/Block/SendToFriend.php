<?php

namespace Drupal\event\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block to send event data to a friend.
 *
 * @Block(
 *  id = "send_to_friend_block",
 *  admin_label = @Translation("Send to Friend Block"),
 *  category = @Translation("Event")
 * )
 */
class SendToFriend extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $formClass = 'Drupal\event\Form\OpenSendToFriendModalForm';

    return \Drupal::formBuilder()->getForm($formClass);
  }

  /**
   * Development.
   */
  public function getCacheMaxAge() {
    return 0;
  }

}

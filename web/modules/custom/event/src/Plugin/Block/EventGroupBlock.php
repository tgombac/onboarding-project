<?php

namespace Drupal\event\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\event\Service\EventGroupInterface;
use Drupal\group\Entity\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that allows users to join / leave an event's group.
 *
 * @Block(
 *   id = "event_group_block",
 *   admin_label = @Translation("Event Group"),
 * )
 */
class EventGroupBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The Event Group service.
   *
   * @var \Drupal\event\Service\EventGroupInterface
   */
  protected EventGroupInterface $eventGroup;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteMatchInterface $routeMatch,
    AccountProxyInterface $currentUser,
    Connection $database,
    EventGroupInterface $eventGroup
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $routeMatch;
    $this->currentUser = $currentUser;
    $this->database = $database;
    $this->eventGroup = $eventGroup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('database'),
      $container->get('event.event_group')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $event = $this->routeMatch->getParameter('node');
    if ($this->currentUser->id() === 0 || $event->bundle() !== 'event') {
      return [];
    }

    $group = $this->eventGroup->getGroup($event);

    if ($group->getMember($this->currentUser)) {
      $link = [
        '#type' => 'link',
        '#title' => $this->t('Leave group'),
        '#url' => Url::fromRoute('entity.group.leave', [
          'group' => $group->id(),
        ]),
        '#attributes' => [
          'target' => '_blank',
        ],
      ];
    }
    else {
      $link = [
        '#type' => 'link',
        '#title' => $this->t('Join group'),
        '#url' => Url::fromRoute('entity.group.join', [
          'group' => $group->id(),
        ]),
        '#attributes' => [
          'target' => '_blank',
        ],
      ];
    }

    return [
      $link,
    ];
  }

}

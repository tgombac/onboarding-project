<?php

namespace Drupal\event\Plugin\Block;


use Drupal\Core\Block\BlockBase;

/**
 * Provides a block to display number of events added to favorites.
 *
 * @Block(
 *  id = "event_favorites_block",
 *  admin_label = @Translation("Event Favorites Block"),
 *  category = @Translation("Event")
 * )
 */
class Favorites extends BlockBase {

  /**
   * Returns the flagCount service.
   */
  public static function flagCountService() {
    return \Drupal::service('flag.count');
  }

  /**
   * Returns the flag service.
   */
  public static function flagService() {
    return \Drupal::service('flag');
  }

  /**
   * Returns the favorite event flag.
   */
  public static function favoriteEventFlag() {
    return self::flagService()->getAllFlags()['favorite_event'];
  }

  /**
   * Fetch count of favorite events and build the block.
   */
  public function build() {
    $count = self::flagCountService()->getUserFlagFlaggingCount(self::favoriteEventFlag(), \Drupal::currentUser());

    return [
      '#theme' => 'event_favorites_block',
      '#count' => $count,
      '#link' => '/my-favorite-events',
    ];
  }

  /**
   * Set the cache tags to favorite event flagging list.
   *
   * Will invalidate cache when the flagging count changes.
   */
  public function getCacheTags() {
    return ['flagging_list:favorite_event'];
  }

  /**
   * Set the cache context to the current user.
   */
  public function getCacheContexts() {
    return ['user'];
  }

}

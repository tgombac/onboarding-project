<?php

namespace Drupal\event\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\event\Service\EventService;

/**
 * Provides an Event Info block.
 *
 * @Block(
 *  id = "event_info_block",
 *  admin_label = @Translation("Event Info Block"),
 *  category = @Translation("Event")
 * )
 */
class EventInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function build(): array {
    $node = \Drupal::routeMatch()->getParameter('node');

    if ($node->getType() !== 'event') {
      throw new \Exception('Node is not of ContentType event');
    }

    $startDateISO = $node->get('field_start_date')->getString();
    $endDateISO   = $node->get('field_end_date')->getString();

    $eventService = \Drupal::getContainer()->get(EventService::class);

    $output = $eventService->getInfoString($startDateISO, $endDateISO);

    return [
      '#theme' => 'event_info_block',
      '#content' => $output,
    ];
  }

  /**
   * Disable block caching.
   */
  public function getCacheMaxAge(): int {
    return 0;
  }

}

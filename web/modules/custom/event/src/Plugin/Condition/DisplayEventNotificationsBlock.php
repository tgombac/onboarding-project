<?php

namespace Drupal\event\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\event\Service\EventService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the "Display Event Notifications Block" Condition.
 *
 * @Condition(
 *   id = "display_event_notifications_block",
 *   label = @Translation("Display event notifications block")
 * )
 */
class DisplayEventNotificationsBlock extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The EventService.
   *
   * @var \Drupal\event\Service\EventService
   */
  protected EventService $eventService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $currentUser, EventService $eventService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $currentUser;
    $this->eventService = $eventService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get(EventService::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'display_event_notifications_block' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['display_event_notifications_block'] = [
      '#title' => $this->t('Display only when user has notifications enabled'),
      '#description' => $this->t('When checked, this block will only be displayed to users who have event notifications enabled.'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['display_event_notifications_block'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['display_event_notifications_block'] = $form_state->getValue('display_event_notifications_block');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    if (!$this->configuration['display_event_notifications_block']) {
      return TRUE;
    }

    $eventNotificationsEnabled = $this->eventService->hasEventNotificationsEnabled($this->currentUser);
    if ($this->isNegated()) {
      return !$eventNotificationsEnabled;
    }
    return $eventNotificationsEnabled;
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): string {
    if (!$this->configuration['display_event_notifications_block']) {
      return $this->t('Not restricted');
    }

    if ($this->isNegated()) {
      return $this->t('Shown only to users who do NOT have event notifications enabled? Makes sense.');
    }

    return $this->t('Shown only to users who have event notifications enabled.');
  }

}

<?php

namespace Drupal\event\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for sending event details to a friend.
 */
class SendToFriendModalForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'send_to_friend_modal_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['#prefix'] = '<div id="send_to_friend_modal_form">';
    $form['#suffix'] = '</div>';

    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['info_message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="info_message"></div>',
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => t("Friend's email"),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => t('Send to friend'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'sendInvitationEmail'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');

    $isEmailValid = \Drupal::service('email.validator')->isValid($email);

    $node = \Drupal::routeMatch()->getParameter('node');

    if (!$isEmailValid) {
      $form_state->setErrorByName('email', $this->t('Please enter a valid email address'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do, we're using AJAX.
  }

  /**
   * Send an event invitation email to the email address specified in the form.
   */
  public function sendInvitationEmail(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node->getType() !== 'event') {
      $messenger = \Drupal::messenger();
      $messenger->addError('Error: Node is not of type Event!');

      $homeUrl = Url::fromRoute('<front>');

      $response->addCommand(new RedirectCommand($homeUrl->toString()));

      return $response;
    }

    if ($form_state->hasAnyErrors()) {
      $replaceCommand = new ReplaceCommand('#send_to_friend_modal_form', $form);

      $response->addCommand($replaceCommand);

      return $response;
    }

    // field_location is an entity reference to the location content type.
    $locationNode = $node->get('field_location')->referencedEntities()[0];
    $locationTitle = $locationNode->get('field_title')->getString();

    // Let's format the date for better readability.
    $startDateISO = $node->get('field_start_date')->getString();
    $startDate = new DrupalDateTime($startDateISO, 'UTC');
    $date = $startDate->format('jS F Y');
    $time = $startDate->format('H:i');

    $eventValues = [
      'title' => $node->get('field_title')->getString(),
      'location_title' => $locationTitle,
      'event_start_date' => $date,
      'event_start_time' => $time,
      'link' => $node->toUrl()->setAbsolute()->toString(),
    ];

    $formValues = $form_state->getValues();

    // All system mails need to specify the module and template key (mirrored
    // from hook_mail()) that the message they want to send comes from.
    $module = 'event';
    $key = 'event_info_mail';

    // Specify 'to' and 'from' email addresses.
    $to = trim($formValues['email']);
    $from = $this->config('system.site')->get('mail');

    // Load context for completion in hook_mail().
    $params = [
      'event_values' => $eventValues,
      'form_values' => $formValues,
    ];

    // Set email language code.
    $languageManager = \Drupal::service('language_manager');
    $languageCode = $languageManager->getDefaultLanguage()->getId();

    $mailManager = \Drupal::service('plugin.manager.mail');

    $titleMessage = $this->t('Success!');
    $message      = $this->t('Event invitation email has been sent.');

    // Send the email now (TRUE param) and display success status to the user.
    $result = $mailManager->mail($module, $key, $to, $languageCode, $params, $from, TRUE);
    if ($result['result'] === FALSE) {
      $titleMessage = $this->t('Failure!');
      $message      = $this->t('There was a problem sending the event info email.');
    }

    $modalOptions = ['width' => 800];

    $openResultModal = new OpenModalDialogCommand($titleMessage, $message, $modalOptions);

    $response->addCommand($openResultModal);

    return $response;
  }

}

<?php

namespace Drupal\event\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for opening SendToFriendModalForm.
 */
class OpenSendToFriendModalForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'open_send_to_friend_modal_form';
  }

  /**
   * Builds the form with the button for opening SendToFriendModalForm.
   *
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $node = \Drupal::routeMatch()->getParameter('node');

    $url = Url::fromRoute('event.open_modal_form', ['node' => $node->id()]);

    $form['open_modal'] = [
      '#type' => 'link',
      '#title' => $this->t('Send this event to a friend'),
      '#url' => $url,
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do, we're using AJAX.
  }

}

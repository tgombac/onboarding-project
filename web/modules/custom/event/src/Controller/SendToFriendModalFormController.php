<?php

namespace Drupal\event\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a callback for opening the modal form.
 */
class SendToFriendModalFormController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The SendToFriendModalFormController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   */
  public function __construct(FormBuilder $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('form_builder'));
  }

  /**
   * Callback for opening the modal form.
   */
  public function openModalForm(NodeInterface $node): AjaxResponse {
    $response = new AjaxResponse();

    $modalForm = $this->formBuilder->getForm('Drupal\event\Form\SendToFriendModalForm');

    $dialogOptions = ['width' => 800];

    $openCommand = new OpenModalDialogCommand($this->t('Send to friend'), $modalForm, $dialogOptions);

    $response->addCommand($openCommand);

    return $response;
  }

}

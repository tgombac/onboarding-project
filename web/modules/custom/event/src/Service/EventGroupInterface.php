<?php

namespace Drupal\event\Service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;

/**
 * Provides an event group service interface for working with event groups.
 */
interface EventGroupInterface extends ContainerInjectionInterface {

  /**
   * Gets the group for the provided event.
   *
   * @param \Drupal\node\NodeInterface $event
   *   The event.
   *
   * @return \Drupal\group\Entity\GroupInterface
   *   The group.
   */
  public function getGroup(NodeInterface $event): GroupInterface;

}

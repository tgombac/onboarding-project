<?php

namespace Drupal\event\Service;

use Drupal\Core\Database\Connection;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Event Group service.
 */
class EventGroup implements EventGroupInterface {

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Constructs the EventGroup service.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup(NodeInterface $event): GroupInterface {
    $groupQuery = $this->database->select('groups', 'g');
    $groupQuery->join('group_content_field_data', 'gc', 'g.id = gc.gid');
    $groupQuery->condition('gc.entity_id', $event->id());
    $groupQuery->fields('g', ['id']);

    $groupId = $groupQuery->execute()->fetch()->id;
    return Group::load($groupId);
  }

}

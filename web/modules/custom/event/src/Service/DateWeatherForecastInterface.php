<?php

namespace Drupal\event\Service;

/**
 * Provides an interface for retrieving the weather info for a specific date.
 */
interface DateWeatherForecastInterface {

  /**
   * Retrieve the weather forecast.
   *
   * @param \DateTime $dateUTC
   *   The date to retrieve the forecast for.
   * @param string $latitude
   *   The latitude of the location.
   * @param string $longitude
   *   The longitude of the location.
   *
   * @return string
   *   The weather forecast string.
   */
  public function getWeatherForecast(\DateTime $dateUTC, string $latitude, string $longitude): string;

}

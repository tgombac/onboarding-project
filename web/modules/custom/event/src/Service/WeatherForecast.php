<?php

namespace Drupal\event\Service;

use Dotenv\Dotenv;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a weather forecast implementation using open weather api.
 */
class WeatherForecast implements DateWeatherForecastInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  const BASE_REQUEST_URI = 'https://api.openweathermap.org/data/2.5/forecast';

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The Open Weather API key.
   *
   * @var string
   */
  private string $apiKey;

  /**
   * Construct a WeatherForecast object.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP Client to use for requests.
   */
  public function __construct(ClientInterface $httpClient) {
    $this->httpClient = $httpClient;

    $dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
    $dotenv->load();
    if (isset($_ENV['OPEN_WEATHER_MAP_API_KEY'])) {
      $this->apiKey = $_ENV['OPEN_WEATHER_MAP_API_KEY'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWeatherForecast(\DateTime $dateUTC, string $latitude, string $longitude): string {
    try {
      $forecastData = $this->fetchForecastData($latitude, $longitude);
    }
    catch (\Exception $e) {
      return $this->t('Could not fetch the weather information at this time.');
    }

    try {
      $forecastArray = $this->getForecastArray($forecastData, $dateUTC);
    }
    catch (\Exception $e) {
      return $this->t('Could not fetch the weather information for the specified date.');
    }

    $forecastString = $this->buildForecastString($forecastArray);
    return ucfirst($forecastString) . '.';
  }

  /**
   * Fetch the forecast data.
   *
   * @param string $latitude
   *   The latitude to fetch the data for.
   * @param string $longitude
   *   The longitude to fetch the data for.
   *
   * @return array
   *   The forecast data.
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function fetchForecastData(string $latitude, string $longitude): array {
    $response = $this->httpClient->request('GET', self::BASE_REQUEST_URI,
      $this->getRequestParameters($latitude, $longitude));

    if ($response->getStatusCode() !== 200) {
      throw new \Exception($this->t('Request was unsuccessful.'));
    }

    $responseContents = $response->getBody()->getContents();

    return json_decode($responseContents, TRUE)['list'];
  }

  /**
   * Get the forecast array.
   *
   * @param array $forecastData
   *   The fetched forecast data.
   * @param \DateTime $dateUTC
   *   The date of the forecast entries to return.
   *
   * @return array
   *   The array of the forecast entries for the specified date.
   *
   * @throws \Exception
   */
  protected function getForecastArray(array $forecastData, \DateTime $dateUTC): array {
    $forecastArray = [];
    foreach ($forecastData as $forecastEntry) {
      $entryDateTime = new \DateTime('@' . (string) $forecastEntry['dt']);
      if ($entryDateTime->format('dmY') !== $dateUTC->format('dmY')) {
        continue;
      }

      $weatherEntry = $forecastEntry['weather'][0];
      if (isset($forecastArray[$weatherEntry['id']])) {
        continue;
      }

      $weatherEntry['temp'] = $forecastEntry['main']['temp'];
      $forecastArray[$weatherEntry['id']] = $weatherEntry;
    }

    return $forecastArray;
  }

  /**
   * Uses an ugly approach to build the forecast string.
   *
   * @param array $forecastArray
   *   The forecast array.
   *
   * @return string
   *   The forecast string.
   */
  protected function buildForecastString(array $forecastArray): string {
    $forecastArrayLength = count($forecastArray);
    if ($forecastArrayLength === 1) {
      return array_pop($forecastArray)['description'];
    }

    $forecastDescriptions = array_map(function ($forecastEntry) {
      return $forecastEntry['description'];
    }, $forecastArray);

    if ($forecastArrayLength === 2) {
      return implode(' and ', $forecastDescriptions);
    }

    return implode(', ', array_slice($forecastDescriptions, 0, ($forecastArrayLength - 1)))
      . 'and ' . end($forecastDescriptions);
  }

  /**
   * Build the request parameters.
   *
   * @param string $latitude
   *   The latitude of the location.
   * @param string $longitude
   *   The longitude of the location.
   *
   * @return array
   *   The parameters array.
   */
  protected function getRequestParameters(string $latitude, string $longitude): array {
    return [
      'query' => [
        'lat' => $latitude,
        'lon' => $longitude,
        'units' => 'metric',
        'appid' => $this->apiKey,
      ],
    ];
  }

}

<?php

namespace Drupal\event\Service;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Session\AccountInterface;
use Drupal\message\Entity\Message;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Provides helper functions for working with events.
 */
class EventService {

  /**
   * Creates a notification message for the specified @param $event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateNotificationMessage(Node $event) {
    $recipients = self::getEventNotificationRecipients($event);
    $location   = $this->getEventLocation($event);

    $message = Message::create([
      'template' => 'event_notification',
      'uid' => $event->get('uid'),
    ]);

    $message->set('field_recipients', $recipients);
    $message->set('field_node_reference', $event);
    $message->set('field_location', $location);

    $message->save();
  }

  /**
   * Returns the location Entity of the event.
   */
  public function getEventLocation(Node $event) {
    $locationReference = $event->get('field_location');

    if (count($locationReference->getValue()) > 0) {
      $targetId = $locationReference->getValue()[0]['target_id'];

      return Node::load($targetId);
    }

    return NULL;
  }

  /**
   * Returns a list of users that have 'notify me about new events' checked.
   *
   * Additionally, takes into account the user's event types of interest,
   * if the user is interested in any particular type(s) of events,
   * they'll only receive the notifications for those types of events.
   */
  private static function getEventNotificationRecipients(Node $event) {
    $userIds = \Drupal::entityQuery('user')
      ->condition('field_notify_me_about_new_events', TRUE)
      ->execute();

    $recipients = [];
    foreach ($userIds as $userId) {
      $eventTypesOfInterest = self::getUserEventTypesOfInterest($userId);
      // As the user hasn't selected any particular event types of interest,
      // we're assuming that they're interested in all of them...
      if ($eventTypesOfInterest === NULL) {
        array_push($recipients, $userId);
      }
      else {
        foreach ($eventTypesOfInterest as $eventTypeOfInterest) {
          if ($event->get('field_event_type')->getString() === $eventTypesOfInterest->getString()) {
            array_push($recipients, $userId);
            break;
          }
        }
      }
    }

    return $recipients;
  }

  /**
   * Returns a list of event types that the specified user is interested in.
   */
  private static function getUserEventTypesOfInterest($userId) {
    $user = User::load($userId);

    $eventTypesOfInterest = $user->get('field_event_types_of_interest');

    if (count($eventTypesOfInterest->getValue()) === 0) {
      return NULL;
    }

    return $eventTypesOfInterest;
  }

  /**
   * Returns number of days until specified date.
   *
   * A negative return value means the date has already passed.
   * A return value of zero means the date is today.
   *
   * @throws \Exception
   */
  public function getDaysUntil($dateISO): int {
    $currentDate = new DrupalDateTime('now', 'UTC');
    $date        = new DrupalDateTime($dateISO, 'UTC');

    // We only want the date info and can discard the time.
    $currentDate->setTime(0, 0, 0);
    $date->setTime(0, 0, 0);

    $diff = $currentDate->diff($date)->days;

    if ($currentDate > $date) {
      $diff *= (-1);
    }

    return $diff;
  }

  /**
   * Returns true if the current datetime is between the params.
   *
   * @throws \Exception
   */
  public function isInProgress($startDateISO, $endDateISO): bool {
    $startDate = new DrupalDateTime($startDateISO, 'UTC');
    $endDate = new DrupalDateTime($endDateISO, 'UTC');

    $currentDate = new DrupalDateTime('now', 'UTC');

    if ($currentDate >= $startDate && $currentDate <= $endDate) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Converts daysUntil into a string representation the user can understand.
   *
   * @return string
   *   info message according to the specified dates.
   *
   * @throws \Exception
   */
  public function getInfoString($startDateISO, $endDateISO): string {
    if ($this->isInProgress($startDateISO, $endDateISO)) {
      return 'This event is currently in progress';
    }

    $daysUntil = $this->getDaysUntil($startDateISO);

    if ($daysUntil === 0) {
      return 'This event is happening today';
    }
    elseif ($daysUntil === 1) {
      return 'This event starts tomorrow';
    }
    elseif ($daysUntil > 1) {
      return $daysUntil . ' days left until the event starts';
    }

    return 'This event has already passed';
  }

  /**
   * Converts @param $title.
   *
   * Returns it in Title Case.
   */
  public function convertToTitleCase($title) {
    $titleArray = explode(' ', $title);

    // Articles, conjunctions and prepositions are lower case in Title Case.
    $ignoredWords = ['a', 'an', 'the', 'as', 'at', 'but', 'by', 'for', 'from',
      'if', 'in', 'nor', 'or', 'of', 'off', 'out', 'over', 'to', 'and', 'upon',
      'about', 'on',
    ];

    $lastIndex = count($titleArray) - 1;
    $titleCase = '';
    foreach ($titleArray as $key => $word) {
      $ignoreCurrent = in_array(mb_strtolower($word), $ignoredWords);

      // The FIRST word in Title Case is ALWAYS capitalized.
      if ($key === 0) {
        $titleCase .= mb_convert_case($word, MB_CASE_TITLE);
      }
      // Likewise, the LAST word in Title Case is also ALWAYS capitalized.
      elseif ($key === $lastIndex) {
        $titleCase .= ' ' . mb_convert_case($word, MB_CASE_TITLE);
      }
      elseif ($ignoreCurrent === TRUE) {
        // Make sure to set it to lowercase, as it should be.
        $titleCase .= ' ' . mb_convert_case($word, MB_CASE_LOWER);
      }
      else {
        $titleCase .= ' ' . mb_convert_case($word, MB_CASE_TITLE);
      }
    }

    return $titleCase;
  }

  /**
   * Returns TRUE if the provided user account has event notifications enabled.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user to check.
   *
   * @return bool
   *   The value of the event notifications field.
   */
  public function hasEventNotificationsEnabled(AccountInterface $account): bool {
    $user = User::load($account->id());
    if (!$user) {
      return FALSE;
    }

    if (!$user->hasField('field_notify_me_about_new_events')) {
      return FALSE;
    }

    return (bool) $user->field_notify_me_about_new_events->value;
  }

}

<?php

namespace Drupal\Tests\event\Unit\Service;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Language\Language;
use Drupal\Tests\UnitTestCase;
use Drupal\event\Service\EventService;

/**
 * Unit test class for EventService.
 */
class EventServiceTest extends UnitTestCase {

  /**
   * EventService instance.
   *
   * @var \Drupal\event\Service\EventService
   */
  protected $eventService;

  /**
   * Initialize EventService instance.
   */
  public function setUp(): void {
    $this->eventService = new EventService();

    $container = new ContainerBuilder();

    $languageManagerMock = $this->getMockBuilder('Drupal\Core\Language\LanguageManager')
      ->disableOriginalConstructor()
      ->getMock();
    $languageManagerMock->expects($this->any())
      ->method('getCurrentLanguage')
      ->willReturn(new Language(Language::$defaultValues));

    $container->set('language_manager', $languageManagerMock);

    \Drupal::setContainer($container);
  }

  /**
   * @covers Drupal\event\Service\EventService::convertToTitleCase.
   */
  public function testConvertToTitleCase() {
    $inputs = [
      'snow white and the seven dwarves',
      'newcastle Upon Tyne',
      'brighton ON SEA',
      'the Last of the mohicans',
      'abouT A boY',
      'what is it all about?',
      'How to be Black',
      'it is not only unfair but also illegal',
      'IN THE NAME OF THE FATHER',
      '10 life hacks that will change your life. number 1 WILL shock you!',
      'Alphabet buys GPT3',
      'This Is Correct.',
    ];

    $expectedOutputs = [
      'Snow White and the Seven Dwarves',
      'Newcastle upon Tyne',
      'Brighton on Sea',
      'The Last of the Mohicans',
      'About a Boy',
      'What Is It All About?',
      'How to Be Black',
      'It Is Not Only Unfair but Also Illegal',
      'In the Name of the Father',
      '10 Life Hacks That Will Change Your Life. Number 1 Will Shock You!',
      'Alphabet Buys Gpt3',
      'This Is Correct.',
    ];

    foreach ($inputs as $key => $input) {
      $output = $this->eventService->convertToTitleCase($input);

      $this->assertEquals($expectedOutputs[$key], $output);
    }
  }

  /**
   * @covers Drupal\event\Service\EventService::getDaysUntil.
   *
   * @throws \Exception
   */
  public function testGetDaysUntil() {
    $testDates = [
      '3-days-ago' => gmdate('r', strtotime('- 3 days')),
      'yesterday' => gmdate('r', strtotime('- 1 day')),
      'today' => gmdate('r', strtotime('now')),
      'tomorrow' => gmdate('r', strtotime('+ 1 day')),
      '4-days-from-today' => gmdate('r', strtotime('+ 4 days')),
      '31-days-from-today' => gmdate('r', strtotime('+ 31 days')),
    ];

    $expectedValues = [
      '3-days-ago' => -3,
      'yesterday' => -1,
      'today' => 0,
      'tomorrow' => 1,
      '4-days-from-today' => 4,
      '31-days-from-today' => 31,
    ];

    foreach ($testDates as $key => $date) {
      $this->assertEquals($expectedValues[$key], $this->eventService->getDaysUntil($date));
    }
  }

  /**
   * Cleanup.
   */
  public function tearDown(): void {
    unset($this->eventService);
  }

}

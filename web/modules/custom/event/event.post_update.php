<?php

use Drupal\node\Entity\Node;

/**
 * Implements hook_post_update_NAME().
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function event_post_update_save_all_event_titles_as_title_case() {
  $nids = \Drupal::entityQuery('node')->condition('type', 'event')->execute();

  $events = Node::loadMultiple($nids);

  foreach ($events as $event) {
    $event->save();
  }
}

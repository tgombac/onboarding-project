<?php

namespace Drupal\Tests\ad_site_status_message\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the module configuration form is working correctly.
 *
 * @group ad_site_status_message
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * The theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stable9';

  /**
   * The modules needed to run the test.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'ad_site_status_message',
  ];

  /**
   * The path to the config form.
   *
   * @var \Drupal\Core\Url
   */
  private Url $configFormPath;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->configFormPath = Url::fromRoute('ad_site_status_message.config');
  }

  /**
   * Tests that the user with the correct permission can access the config form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUserWithPermissionCanAccessConfigForm(): void {
    $session = $this->assertSession();

    $user = $this->drupalCreateUser([
      'administer site status message',
    ]);
    $this->drupalLogin($user);

    $this->drupalGet($this->configFormPath);
    $session->statusCodeEquals(200);
  }

  /**
   * Tests that without the proper permission, the user can't access the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUserWithoutPermissionCannotAccessConfigForm(): void {
    $session = $this->assertSession();

    $user = $this->drupalCreateUser([
      'access content',
    ]);
    $this->drupalLogin($user);

    $this->drupalGet($this->configFormPath);
    $session->statusCodeEquals(403);
  }

  /**
   * Tests if valid data results in success.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function testFormSubmitIsSuccessful(): void {
    $session = $this->assertSession();

    $user = $this->drupalCreateUser([
      'administer site status message',
    ]);
    $this->drupalLogin($user);

    $this->drupalGet($this->configFormPath);
    $validData = [
      'message' => 'The site status message',
      'state' => 1,
      'message_type' => 'warning',
    ];
    $this->submitForm($validData, 'Save configuration');

    $this->drupalGet($this->configFormPath);

    $message = $session->fieldExists('message')->getValue();
    $this->assertEquals($validData['message'], $message);

    $state = $session->fieldExists('state')->getValue();
    $this->assertEquals($validData['state'], $state);

    $messageType = $session->fieldExists('message_type')->getValue();
    $this->assertEquals($validData['message_type'], $messageType);
  }

}

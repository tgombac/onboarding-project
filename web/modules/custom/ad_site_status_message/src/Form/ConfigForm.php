<?php

namespace Drupal\ad_site_status_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a configuration form for setting site message.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'ad_site_status_message_config_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('ad_site_status_message.config');

    $state       = $config->get('state');
    $message     = $config->get('message');
    $messageType = $config->get('message_type');

    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#default_value' => ($message === NULL) ? '' : $message,
      '#required' => FALSE,
    ];

    $form['state'] = [
      '#type' => 'radios',
      '#title' => $this->t('State'),
      '#default_value' => ($state === NULL) ? 0 : $state,
      '#options' => [
        0 => $this->t('Disabled'),
        1 => $this->t('Enabled'),
      ],
      '#required' => TRUE,
    ];

    $form['message_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message type'),
      '#default_value' => $messageType,
      '#options' => [
        'plain' => $this->t('Plain'),
        'success' => $this->t('Success'),
        'info' => $this->t('Info'),
        'warning' => $this->t('Warning'),
        'alert' => $this->t('Alert'),
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ad_site_status_message.config');
    $config->set('message', $form_state->getValue('message'));
    $config->set('state', $form_state->getValue('state'));
    $config->set('message_type', $form_state->getValue('message_type'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getEditableConfigNames(): array {
    return [
      'ad_site_status_message.config',
    ];
  }

}

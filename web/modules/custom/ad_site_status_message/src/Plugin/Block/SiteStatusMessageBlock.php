<?php

namespace Drupal\ad_site_status_message\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for displaying the status message.
 *
 *  @Block(
 *  id = "ad_site_status_message_block",
 *  admin_label = @Translation("AD Site Status Message Block"),
 *  category = @Translation("Custom")
 * )
 */
class SiteStatusMessageBlock extends BlockBase {

  const CONFIG_NAME = 'ad_site_status_message.config';

  /**
   * Returns configFactory service.
   */
  public static function configFactory() {
    return \Drupal::service('config.factory');
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $config = self::configFactory()->getEditable(self::CONFIG_NAME);

    if ($config->get('state') === "1") {
      return [
        '#theme' => 'ad_site_status_message',
        '#message' => $config->get('message'),
        '#class' => $config->get('message_type'),
        '#attached' => [
          'library' => [
            'ad_site_status_message/site-status-message-block',
          ],
        ],
      ];
    }

    return [];
  }

  /**
   * Cache until the configuration changes.
   */
  public function getCacheTags() {
    $config = self::configFactory()->get(self::CONFIG_NAME);

    return $config->getCacheTags();
  }

}

<?php

use Drupal\review\Entity\Review;

/**
 * Implements hook_post_update_NAME().
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function review_post_update_9002(&$sandbox): void {
  if (!isset($sandbox['total'])) {
    $reviewIds = \Drupal::entityQuery('review')->execute();
    $sandbox['total'] = count($reviewIds);
    $sandbox['current'] = 0;

    if (empty($sandbox['total'])) {
      $sandbox['#finished'] = 1;
      return;
    }
  }

  $reviewsPerBatch = 25;
  $reviewIds = \Drupal::entityQuery('review')
    ->range($sandbox['current'], $reviewsPerBatch)
    ->execute();
  if (empty($reviewIds)) {
    $sandbox['#finished'] = 1;
    return;
  }

  if (!defined('RATING_UPDATE_MULTIPLIER')) {
    define('RATING_UPDATE_MULTIPLIER', 2);
  }
  $reviews = Review::loadMultiple($reviewIds);
  foreach ($reviews as $review) {
    $currentRating = $review->getRating();
    $review->set('rating', $currentRating * RATING_UPDATE_MULTIPLIER);
    $review->save();

    $sandbox['current']++;
  }

  if ($sandbox['current'] >= $sandbox['total']) {
    $sandbox['#finished'] = 1;
    return;
  }

  $sandbox['#finished'] = $sandbox['current'] / $sandbox['total'];
}

<?php

namespace Drupal\Tests\review\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\review\Entity\Review;
use Drupal\review\Entity\ReviewType;
use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Provides tests for the Review entity type CRUD operations.
 */
class ReviewCrudTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'node',
    'text',
    'review',
  ];

  /**
   * The machine name of the review type.
   *
   * @var string
   */
  protected string $reviewTypeMachineName;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('review');

    $this->installSchema('node', ['node_access']);

    $this->installConfig(['node']);

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->reviewTypeMachineName = $this->randomMachineName();
    $reviewType = ReviewType::create([
      'id' => $this->reviewTypeMachineName,
      'label' => $this->randomString(),
    ]);
    $reviewType->save();

    // The Review::postSave method issues a redirect,
    // which causes trouble in testing.
    $this->container->removeDefinition('redirect_response_subscriber');
  }

  /**
   * Test CRUD operations on the Review entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testReviewCrud(): void {
    $this->assertEquals(0, count($this->loadReviews()));

    $review = Review::create([
      'bundle' => $this->reviewTypeMachineName,
      'rating' => 3,
    ]);
    $review->save();
    $this->assertEquals(1, count($this->loadReviews()));

    $review = $this->loadReviews()[$review->id()];
    $this->assertEquals(3, $review->rating->value);

    $review->rating->value = 4;
    $review->save();
    $review = $this->loadReviews()[$review->id()];
    $this->assertEquals(4, $review->rating->value);

    $review->delete();
    $this->assertEquals(0, count($this->loadReviews()));
  }

  /**
   * Load reviews of the class' defined bundle.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function loadReviews(): array {
    return $this->entityTypeManager->getStorage('review')->loadByProperties([
      'bundle' => $this->reviewTypeMachineName,
    ]);
  }

}

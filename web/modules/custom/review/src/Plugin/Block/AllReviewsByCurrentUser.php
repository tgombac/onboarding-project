<?php

namespace Drupal\review\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Provides a Reviews by current user block.
 *
 * @Block(
 *   id = "review_all_reviews_by_current_user",
 *   admin_label = @Translation("All Reviews by Current user"),
 *   category = @Translation("Custom")
 * )
 */
class AllReviewsByCurrentUser extends BlockBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function build(): array|NULL {
    $currentUser = \Drupal::currentUser();
    if ($currentUser->id() == '0') {
      return NULL;
    }

    return [
      'build' => [
        '#lazy_builder' => [
          static::class . '::lazyReviewsByCurrentUser', [
            $currentUser->id(),
          ],
        ],
        '#create_placeholder' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return ['lazyReviewsByCurrentUser'];
  }

  /**
   * Lazy build the reviews by the specified user.
   *
   * @param string $userId
   *   The user id to fetch the reviews of.
   *
   * @return array
   *   The render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public static function lazyReviewsByCurrentUser(string $userId): array {
    $entityTypeManager = \Drupal::entityTypeManager();
    $reviewsByCurrentUser = $entityTypeManager->getStorage('review')
      ->loadByProperties([
        'uid' => $userId,
      ]);
    if (empty($reviewsByCurrentUser)) {
      return [];
    }

    $user = User::load($userId);
    $reviewListItems = [];
    foreach ($reviewsByCurrentUser as $review) {
      $targetNodeId = $review->get('target_entity')->target_id;
      $targetNode = Node::load($targetNodeId);

      $titleParams = [
        '%reviewId' => $review->id(),
        '%reviewTargetContentType' => ucfirst($targetNode->bundle()),
        '%reviewTargetTitle' => $targetNode->getTitle(),
      ];
      $title = t('Review %reviewId for %reviewTargetContentType: %reviewTargetTitle', $titleParams);

      $reviewListItems[] = [
        '#type' => 'link',
        '#title' => $title,
        '#url' => $review->toUrl(),
      ];
    }

    sleep(2);

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => 'Reviews of ' . $user->getDisplayName(),
      '#items' => $reviewListItems,
    ];
  }

}

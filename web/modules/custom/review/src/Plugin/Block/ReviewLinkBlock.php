<?php

namespace Drupal\review\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a Review link block.
 *
 * @Block(
 *   id = "review_review_link",
 *   admin_label = @Translation("Review Link"),
 *   category = @Translation("Custom")
 * )
 */
class ReviewLinkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array|NULL {
    $routeMatch = \Drupal::routeMatch();
    if ($routeMatch->getRouteName() !== 'entity.node.canonical') {
      return NULL;
    }

    $node = \Drupal::routeMatch()->getParameter('node');
    $contentType = $node->bundle();
    $allowedContentTypes = [
      'company',
      'location',
      'event',
    ];
    if (!in_array($contentType, $allowedContentTypes)) {
      return NULL;
    }

    // Review types were set up to have the same type machine names as their
    // Node equivalents.
    $link = '/review/add/' . $contentType;
    $url = Url::fromUserInput($link)->setRouteParameter('node_id', $node->id());

    return [
      '#type' => 'link',
      '#title' => $this->t('Add review'),
      '#url' => $url,
    ];
  }

}

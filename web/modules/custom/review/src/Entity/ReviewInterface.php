<?php

namespace Drupal\review\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\migrate\Plugin\migrate\destination\Entity;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Review entity.
 */
interface ReviewInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Returns the Review's rating.
   *
   * @return int
   *   The rating value.
   */
  public function getRating(): int;

  /**
   * Get the Entity the Review targets (is for).
   *
   * @return \Drupal\migrate\Plugin\migrate\destination\Entity
   *   The reviewed Entity.
   */
  public function getTargetEntity(): Entity;

}

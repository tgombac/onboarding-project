<?php

namespace Drupal\review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\migrate\Plugin\migrate\destination\Entity;
use Drupal\user\EntityOwnerTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines the Review entity.
 *
 * @ContentEntityType(
 *   id = "review",
 *   label = @Translation("Review"),
 *   base_table = "review",
 *   data_table = "review_field_data",
 *   entity_keys = {
 *      "id" = "id",
 *      "uuid" = "uuid",
 *      "bundle" = "bundle",
 *      "owner" = "uid",
 *   },
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\review\Entity\ReviewListBuilder",
 *   },
 *   links = {
 *      "canonical" = "/review/{review}",
 *      "add-page" = "/review/add",
 *      "add-form" = "/review/add/{review_type}",
 *      "edit-form" = "/review/{review}/edit",
 *      "delete-form" = "/review/{review}/delete",
 *      "collection" = "/admin/content/reviews",
 *   },
 *   admin_permission = "administer review types",
 *   bundle_entity_type = "review_type",
 *   field_ui_base_route = "entity.review_type.edit_form",
 * )
 */
class Review extends ContentEntityBase implements ReviewInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   *
   * Set current user the owner of the review.
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!$this->getOwnerId()) {
      $this->setOwnerId(\Drupal::currentUser()->id());
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['rating'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Rating'))
      ->setDescription(t('The Rating value of the Review.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setPropertyConstraints('value', [
        'Range' => [
          'min' => 1,
          'max' => 10,
        ],
      ])
      ->setSettings([
        'default_value' => NULL,
        'min' => 1,
        'max' => 10,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -1,
      ]);

    $fields['target_entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Target Entity'))
      ->setDescription(t('The Target Entity of the Review.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setSettings([
        'target_type' => 'node',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
      ]);

    $fields['review_summary'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Review Summary'))
      ->setDescription(t('The Summary of this Review'))
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'basic_string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 5,
        ],
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getRating(): int {
    return $this->get('rating')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntity(): Entity {
    return $this->get('target_entity')->entity;
  }

}

<?php

namespace Drupal\review\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ReviewType entity.
 *
 * @ConfigEntityType(
 *   id = "review_type",
 *   label = @Translation("Review type"),
 *   bundle_of = "review",
 *   entity_keys = {
 *      "id" = "id",
 *      "label" = "label",
 *   },
 *   config_prefix = "type",
 *   config_export = {
 *      "id",
 *      "label",
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\review\Form\ReviewTypeEntityForm",
 *       "edit" = "Drupal\review\Form\ReviewTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\review\Entity\ReviewTypeListBuilder",
 *   },
 *   admin_permission = "administer review types",
 *   links = {
 *     "add-form" = "/admin/structure/review_types/add",
 *     "edit-form" = "/admin/structure/review_types/{review_type}/edit",
 *     "delete-form" = "/admin/structure/review_types/{review_type}/delete",
 *     "collection" = "/admin/structure/review_types"
 *   }
 * )
 */
class ReviewType extends ConfigEntityBundleBase {

  /**
   * Machine name of the ReviewType.
   *
   * @var string
   */
  protected string $id;

  /**
   * Label of the ReviewType.
   *
   * @var string
   */
  protected string $label;

}

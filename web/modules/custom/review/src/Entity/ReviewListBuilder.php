<?php

namespace Drupal\review\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\node\Entity\Node;

/**
 * Provides a list builder for the Review entity.
 */
class ReviewListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $row['review_id'] = $this->t('Review ID');
    $row['type'] = $this->t('Type');
    $row['author'] = $this->t('Author');
    $row['rating'] = $this->t('Rating');
    $row['target_node'] = $this->t('Target Node');

    return $row + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $targetNodeId = $entity->get('target_entity')->target_id;
    $targetNode = Node::load($targetNodeId);

    $row['review_id']['data'] = [
      '#type' => 'link',
      '#title' => $entity->id(),
      '#url' => $entity->toUrl(),
    ];
    $row['type']['data'] = $entity->bundle();
    $row['author']['data'] = $entity->getOwner()->get('name')->value;
    $row['rating']['data'] = $entity->getRating();
    $row['target_node']['data'] = [
      '#type' => 'link',
      '#title' => $targetNode->label(),
      '#url' => $targetNode->toUrl(),
    ];

    return $row + parent::buildRow($entity);
  }

}

<?php

namespace Drupal\review\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an EntityForm for the ReviewType entity.
 */
class ReviewTypeEntityForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity->label(),
      '#description' => $this->t('Label of the Review type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\review\Entity\ReviewType::load',
      ],
      '#disabled' => !$entity->isNew(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    $messenger = \Drupal::messenger();
    $messageParams = [
      '%label' => $entity->label(),
    ];
    switch ($status) {
      case SAVED_NEW:
        $messenger->addMessage($this->t('Created the %label Review type.',
          $messageParams));
        break;

      default:
        $messenger->addMessage($this->t('Saved the %label Review type.',
          $messageParams));
        break;
    }

    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }

}

<?php

namespace Drupal\Tests\equipment\Kernel;

use Drupal\equipment\Service\EquipmentRating;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Provides tests for evaluating equipment rating calculation.
 */
class EquipmentRatingTest extends KernelTestBase {

  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'menu_ui',
    'taxonomy',
    'node',
    'text',
    'equipment',
    'location',
  ];

  /**
   * The available equipment term ids.
   *
   * @var array
   */
  protected array $availableEquipmentTermIds;

  /**
   * The equipment rating service.
   *
   * @var \Drupal\equipment\Service\EquipmentRating
   */
  protected EquipmentRating $equipmentRatingService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');

    $this->installSchema('node', ['node_access']);

    $this->installConfig([
      'node',
      'equipment',
      'location',
    ]);

    $starRequirements = [1, 1, 1, 2, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5];
    foreach ($starRequirements as $starRequirement) {
      $this->availableEquipmentTermIds[] = $this->createEquipment($starRequirement);
    }

    $this->equipmentRatingService = $this->container->get('equipment.rating');
  }

  /**
   * Tests the equipment rating calculation.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRatingCalculation(): void {
    $equipmentIndexes = [
      [0, 5, 15, 4, 3],
      [1, 3, 2, 4, 5, 6, 7, 15],
      [4, 3, 15, 13, 2, 1, 0],
      [15, 14, 13, 12, 11, 10],
      [0, 1, 2],
      [0, 1, 2, 3],
      [0, 1, 2, 3, 4, 6],
      [0, 1, 2, 3, 4, 5, 6],
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
      [],
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      [0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11],
    ];
    $expectedRatings = [0, 0, 2, 0, 1, 2, 2, 3, 5, 0, 4, 3, 1];

    $termIdsArray = array_map(function ($equipmentIndexes) {
      return array_map(function ($equipmentIndex) {
        return $this->availableEquipmentTermIds[$equipmentIndex];
      }, $equipmentIndexes);
    }, $equipmentIndexes);

    $locationNodes = [];
    foreach ($termIdsArray as $termIds) {
      $locationNodes[] = $this->generateTestLocationNode($termIds);
    }

    foreach ($locationNodes as $index => $locationNode) {
      $this->assertEquals(
        $expectedRatings[$index],
        $this->equipmentRatingService->getLocationRating($locationNode->id())
      );
    }
  }

  /**
   * Tests the equipment rating service with a non-existent node.
   */
  public function testNonExistentLocationNode(): void {
    $this->expectException(\InvalidArgumentException::class);
    $this->equipmentRatingService->getLocationRating(5);
  }

  /**
   * Tests the equipment rating service with a node that's not of type location.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testNotLocationNode(): void {
    $this->createContentType([
      'type' => 'not_location',
    ]);

    $notLocationNode = Node::create([
      'type' => 'not_location',
      'title' => $this->randomString(),
    ]);
    $notLocationNode->save();

    $this->expectException(\InvalidArgumentException::class);
    $this->equipmentRatingService->getLocationRating($notLocationNode->id());
  }

  /**
   * Generate a test location node.
   *
   * @param array $equipmentTermIds
   *   The equipment term ids of the location.
   *
   * @return \Drupal\node\NodeInterface
   *   The generated location node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function generateTestLocationNode(array $equipmentTermIds): NodeInterface {
    $location = Node::create([
      'type' => 'location',
      'title' => $this->randomString(),
      'field_equipment' => $equipmentTermIds,
    ]);
    $location->save();

    return $location;
  }

  /**
   * Create and save equipment with the provided star requirement.
   *
   * @param int $starRequirement
   *   The star requirement.
   *
   * @return int
   *   The created equipment term's id.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createEquipment(int $starRequirement): int {
    $equipment = Term::create([
      'vid' => 'equipment',
      'name' => $this->randomString(),
      'field_star_requirement' => $starRequirement,
    ]);
    $equipment->save();

    return $equipment->id();
  }

}

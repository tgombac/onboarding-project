<?php

namespace Drupal\equipment\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin implementation of the 'title_and_icon' formatter.
 *
 * @FieldFormatter(
 *   id = "title_and_icon",
 *   label = @Translation("Title and icon"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TitleAndIconFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => 'icon',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable(),
    ];

    return $element;
  }

  /**
   * Dirty implementation of providing title and icon on equipment taxonomy.
   *
   * There must exist a better way of doing this.
   *
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $title = $entity->get('name')->getString();
      $icon = $entity->get('field_icon');

      $entityRepository = \Drupal::service('entity.repository');

      // Get default icon.
      $iconSettings    = $icon->getFieldDefinition()->getSettings();
      $defaultIconUuid = $iconSettings['default_image']['uuid'];
      $defaultIcon     = $entityRepository->loadEntityByUuid('file', $defaultIconUuid);

      // Set default icon URI.
      $uri = $defaultIcon->getFileUri();
      if (count($icon) > 0) {
        $fileId = $icon[0]->getValue()['target_id'];

        $uri = File::load($fileId)->getFileUri();
      }

      $imageStyleSetting = $this->getSetting('image_style');
      $imageStyle        = ImageStyle::load($imageStyleSetting);

      $url = $imageStyle->buildUrl($uri);

      $elements[$delta] = [
        '#theme' => 'title_and_icon_formatter',
        '#title' => $title,
        '#icon' => $url,
      ];
    }

    return $elements;
  }

}

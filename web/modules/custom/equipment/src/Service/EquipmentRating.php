<?php

namespace Drupal\equipment\Service;

use Drupal\node\Entity\Node;

/**
 * Provides a service for calculating Location rating based on equipment.
 */
class EquipmentRating {

  /**
   * Returns the rating for the specified @param $locationNodeId.
   */
  public function getLocationRating($locationNodeId) {
    $rating = 0;

    while ($this->locationSatisfies($locationNodeId, ($rating + 1)) && $rating < 5) {
      $rating++;
    }

    return $rating;
  }

  /**
   * Returns true if the location satisfies the specified @param $rating.
   */
  private function locationSatisfies($locationNodeId, $rating) {
    if (Node::load($locationNodeId) === NULL) {
      throw new \InvalidArgumentException('No node with the specified id.');
    }

    $requiredEquipment  = $this->getEquipmentArray($rating);
    $availableEquipment = $this->getLocationEquipment($locationNodeId);

    $diff = array_diff($requiredEquipment, $availableEquipment);

    return count($diff) === 0;
  }

  /**
   * Returns an array of equipment for the specified @param $locationNodeId.
   */
  private function getLocationEquipment($locationNodeId) {
    $location = Node::load($locationNodeId);

    if ($location->getType() !== 'location') {
      throw new \InvalidArgumentException('The node is not of type location!');
    }

    $equipment = $location->get('field_equipment')->getValue();

    // Match the output of getEquipmentArray.
    $ids = [];
    foreach ($equipment as $equipmentSingle) {
      $id = $equipmentSingle['target_id'];

      $ids[$id] = $id;
    }

    return $ids;
  }

  /**
   * Returns an array of equipment required for the specified @param $rating.
   */
  private function getEquipmentArray($rating) {
    $equipmentQuery = \Drupal::entityQuery('taxonomy_term');
    $equipmentQuery->accessCheck();
    $equipmentQuery->condition('vid', 'equipment');
    $equipmentQuery->condition('field_star_requirement', $rating);

    return $equipmentQuery->execute();
  }

}

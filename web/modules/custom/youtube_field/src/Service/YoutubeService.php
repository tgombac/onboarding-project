<?php

namespace Drupal\youtube_field\Service;

/**
 * Provides helper functions for working with YouTube.
 */
class YoutubeService {

  /**
   * Returns an embeddable YouTube URL for the provided @param $youtubeUrl.
   */
  public function getEmbeddableYoutubeUrl($youtubeUrl): string {
    $videoId = explode('v=', $youtubeUrl)[1];
    $videoId = explode('&', $videoId)[0];

    return 'https://www.youtube.com/embed/' . $videoId . '?feature=oembed';
  }

}

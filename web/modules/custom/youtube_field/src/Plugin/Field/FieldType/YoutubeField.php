<?php

namespace Drupal\youtube_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for storing YouTube links.
 *
 * @FieldType(
 *   id = "youtube_field",
 *   label = @Translation("Youtube Field"),
 *   category = @Translation("YouTube"),
 *   default_formatter = "youtube_field_link_formatter",
 *   default_widget = "youtube_field_widget",
 * )
 */
class YoutubeField extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'link' => [
          'type' => 'text',
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['link'] = DataDefinition::create('string');

    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    $link = $this->get('link')->getValue();

    return ($link === NULL || $link === '');
  }

}

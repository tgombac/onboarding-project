<?php

namespace Drupal\youtube_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'youtube_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "youtube_field_widget",
 *   module = "youtube_field",
 *   label = @Translation("YouTube Field Widget"),
 *   field_types = {
 *     "youtube_field"
 *   }
 * )
 */
class YoutubeFieldWidget extends WidgetBase {

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $link = isset($items[$delta]->link) ? $items[$delta]->link : '';

    $element += [
      'link' => [
        '#type' => 'textfield',
        '#default_value' => $link,
        '#element_validate' => [
          [$this, 'validate'],
        ],
      ],
    ];

    return $element;
  }

  /**
   * Validate the link field.
   */
  public function validate($element, FormStateInterface $form_state) {
    $link = $element['#value'];

    if (strlen($link) === 0) {
      $form_state->setValueForElement($element, '');
      return;
    }

    $youtubeLinkPattern = '/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/';

    if (!preg_match($youtubeLinkPattern, $link)) {
      $form_state->setError($element, $this->t('Enter a valid YouTube link!'));
    }
  }

}

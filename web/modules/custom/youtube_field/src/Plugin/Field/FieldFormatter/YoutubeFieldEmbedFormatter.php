<?php

namespace Drupal\youtube_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\youtube_field\Service\YoutubeService;

/**
 * Plugin implementation of the 'youtube_field_embed_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "youtube_field_embed_formatter",
 *   module = "youtube_field",
 *   label = @Translation("Youtube Embed"),
 *   field_types = {
 *     "youtube_field"
 *   }
 * )
 */
class YoutubeFieldEmbedFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $youtubeService = \Drupal::getContainer()->get(YoutubeService::class);

      $url = $youtubeService->getEmbeddableYoutubeUrl($item->link);

      $elements[$delta] = [
        '#theme' => 'youtube-embed',
        '#link'  => $url,
      ];
    }

    return $elements;
  }

}

<?php

namespace Drupal\youtube_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'youtube_field_link_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "youtube_field_link_formatter",
 *   module = "youtube_field",
 *   label = @Translation("Youtube Link"),
 *   field_types = {
 *     "youtube_field"
 *   }
 * )
 */
class YoutubeFieldLinkFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return [
      'open_in_new_tab' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['open_in_new_tab'] = [
      '#title' => t('Link settings'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('open_in_new_tab'),
      '#options' => [
        TRUE => $this->t('Open link in new tab'),
        FALSE => $this->t('Open link in the same tab'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $openInNewTabSetting = $this->getSetting('open_in_new_tab');

    $target = ($openInNewTabSetting) ? '_blank' : '_self';

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'a',
        '#attributes' => [
          'href' => $item->link,
          'target' => $target,
        ],
        '#value' => $item->link,
      ];
    }

    return $elements;
  }

}

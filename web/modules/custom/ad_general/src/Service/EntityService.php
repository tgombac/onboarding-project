<?php

namespace Drupal\ad_general\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\node\Entity\Node;

/**
 * Provides helper functions for working with entities.
 */
class EntityService {

  /**
   * Returns taxonomy term names for the specified vocabulary.
   */
  public function loadTermNames($vocabularyId) {
    try {
      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vocabularyId);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return [];
    }

    $termNames = [];
    foreach ($terms as $term) {
      $termNames[] = $term->name;
    }

    return $termNames;
  }

  /**
   * Query and return taxonomy ids.
   */
  public function getTaxonomyIds($vocabularyId, $termName) {
    $taxonomyQuery = \Drupal::entityQuery('taxonomy_term');
    $taxonomyQuery->condition('vid', $vocabularyId);
    $taxonomyQuery->condition('name', $termName);

    return $taxonomyQuery->execute();
  }

  /**
   * Query and return filtered nodes.
   */
  public function getNodes($contentType, $vocabularyId, $taxonomyFieldName, $term = '', $title = '') {
    $query = \Drupal::entityQuery('node');
    $query->condition('type', $contentType);

    if ($term) {
      // Returns array, but we're expecting only one element.
      $termId = $this->getTaxonomyIds($vocabularyId, $term);

      // We might get zero results from the taxonomy_term query,
      // in that case, we should avoid adding an empty condition.
      if ($termId) {
        $query->condition($taxonomyFieldName, $termId);
      }
    }

    if ($title) {
      $valueCondition = '%' . $title . '%';
      $query->condition('field_title', $valueCondition, 'LIKE');
    }

    $nids = $query->execute();

    return Node::loadMultiple($nids);
  }

  /**
   * Return a list of nodes with link and title.
   */
  public function listNodes($nodes) {
    $content = [];
    foreach ($nodes as $node) {
      $link = $node->toUrl()->toString();
      $title = $node->get('field_title')->getString();

      $content[] = [
        'link' => $link,
        'title' => $title,
      ];
    }

    return [
      '#theme' => 'node_list',
      '#content' => $content,
    ];
  }

}

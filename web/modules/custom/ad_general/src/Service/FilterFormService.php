<?php

namespace Drupal\ad_general\Service;

/**
 * Provides helper functions for generating filter forms.
 */
class FilterFormService {

  /**
   * Generates a filter form.
   */
  public function generateFilterForm($form, $vocabularyId, $vocabularyFilterTitle): array {
    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => t('Filter'),
      '#open' => TRUE,
    ];

    $form['filters']['title'] = [
      '#title' => t('Filter by title'),
      '#type' => 'search',
    ];

    $entityService = \Drupal::getContainer()->get(EntityService::class);

    $termNames = $entityService->loadTermNames($vocabularyId);

    $form['filters'][$vocabularyId] = [
      '#title' => t('Filter by') . ' ' . $vocabularyFilterTitle,
      '#type' => 'select',
      '#options' => $termNames,
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
    ];

    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Filter'),
    ];

    return $form;
  }

}

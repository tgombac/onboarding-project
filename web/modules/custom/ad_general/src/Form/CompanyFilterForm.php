<?php

namespace Drupal\ad_general\Form;

use Drupal\ad_general\Service\FilterFormService;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form to filter companies.
 */
class CompanyFilterForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'company_filter_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filterFormService = \Drupal::getContainer()->get(FilterFormService::class);

    return $filterFormService->generateFilterForm($form, 'industry', 'industry');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title         = $form_state->getValue('title');
    $industryValue = $form_state->getValue('industry');

    $industry = $form['filters']['industry']['#options'][$industryValue];

    $url = Url::fromRoute('ad_general.companies');
    $url->setRouteParameter('title', $title);
    $url->setRouteParameter('industry', $industry);

    $form_state->setRedirectUrl($url);
  }

}

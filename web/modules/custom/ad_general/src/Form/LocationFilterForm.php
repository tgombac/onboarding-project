<?php

namespace Drupal\ad_general\Form;

use Drupal\ad_general\Service\FilterFormService;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form to filter companies.
 */
class LocationFilterForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'location_filter_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filterFormService = \Drupal::getContainer()->get(FilterFormService::class);

    return $filterFormService->generateFilterForm($form, 'equipment', 'equipment');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title          = $form_state->getValue('title');
    $equipmentValue = $form_state->getValue('equipment');

    $equipment = $form['filters']['equipment']['#options'][$equipmentValue];

    $url = Url::fromRoute('ad_general.locations');
    $url->setRouteParameter('title', $title);
    $url->setRouteParameter('equipment', $equipment);

    $form_state->setRedirectUrl($url);
  }

}

<?php

namespace Drupal\ad_general\Form;

use Drupal\ad_general\Service\FilterFormService;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form to filter companies.
 */
class EventFilterForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'event_filter_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filterFormService = \Drupal::getContainer()->get(FilterFormService::class);

    return $filterFormService->generateFilterForm($form, 'event_type', 'event type');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title          = $form_state->getValue('title');
    $eventTypeValue = $form_state->getValue('event_type');

    $industry = $form['filters']['event_type']['#options'][$eventTypeValue];

    $url = Url::fromRoute('ad_general.events');
    $url->setRouteParameter('title', $title);
    $url->setRouteParameter('event_type', $industry);

    $form_state->setRedirectUrl($url);
  }

}

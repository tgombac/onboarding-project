<?php

namespace Drupal\ad_general\Controller;

use Drupal\ad_general\Service\EntityService;
use Drupal\Core\Controller\ControllerBase;

/**
 * Lists nodes.
 */
class NodeController extends ControllerBase {

  /**
   * List all companies.
   */
  public function listCompanies() {
    $requestQuery = \Drupal::request()->query;

    $title = $requestQuery->get('title');
    $industry = $requestQuery->get('industry');

    $output['form'] = $this->formBuilder()->getForm('Drupal\ad_general\Form\CompanyFilterForm');

    $entityService = \Drupal::getContainer()->get(EntityService::class);

    $companies = $entityService->getNodes('company', 'industry', 'field_industry', $industry, $title);

    $output['list'] = $entityService->listNodes($companies);

    return $output;
  }

  /**
   * List all events.
   */
  public function listEvents() {
    $requestQuery = \Drupal::request()->query;

    $title = $requestQuery->get('title');
    $eventType = $requestQuery->get('event_type');

    $output['form'] = $this->formBuilder()->getForm('Drupal\ad_general\Form\EventFilterForm');

    $entityService = \Drupal::getContainer()->get(EntityService::class);

    $events = $entityService->getNodes('event', 'event_type', 'field_event_type', $eventType, $title);

    $output['list'] = $entityService->listNodes($events);

    return $output;
  }

  /**
   * List all locations.
   */
  public function listLocations() {
    $requestQuery = \Drupal::request()->query;

    $title = $requestQuery->get('title');
    $equipment = $requestQuery->get('equipment');

    $output['form'] = $this->formBuilder()->getForm('Drupal\ad_general\Form\LocationFilterForm');

    $entityService = \Drupal::getContainer()->get(EntityService::class);

    $locations = $entityService->getNodes('location', 'equipment', 'field_equipment', $equipment, $title);

    $output['list'] = $entityService->listNodes($locations);

    return $output;
  }

}

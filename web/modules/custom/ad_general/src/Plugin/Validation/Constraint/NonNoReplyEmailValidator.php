<?php

namespace Drupal\ad_general\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that the email doesn't start with no-reply.
 */
class NonNoReplyEmailValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (count($value->getValue()) < 1) {
      return;
    }

    $email = $value->getValue()[0]['value'];

    if (str_starts_with($email, 'no-reply')) {
      $this->context->addViolation($constraint->noReplyEmail);
    }
  }

}

<?php

namespace Drupal\ad_general\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted email doesn't start with 'no-reply'.
 *
 * @Constraint(
 *   id = "NonNoReplyEmail",
 *   label = @Translation("Non no-reply Email", context = "Validation"),
 *   type = "string"
 * )
 */
class NonNoReplyEmail extends Constraint {

  /**
   * Message to show when the submitted email starts with no-reply.
   *
   * @var string
   */
  public string $noReplyEmail = 'Please enter a valid email.';

}

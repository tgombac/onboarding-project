<?php

namespace Drupal\company\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds company has upcoming events to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "add_company_has_upcoming_events",
 *   label = @Translation("Company Has Upcoming Events"),
 *   description = @Translation("Adds company has upcoming events to the indexed data."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class HasUpcomingEvents extends ProcessorPluginBase {

  const PROPERTY_PATH = 'company_has_upcoming_events';

  /**
   * {@inheritDoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Has upcoming events'),
        'description' => $this->t('The company has upcoming events'),
        'type' => 'boolean',
        'processor_id' => $this->getPluginId(),
      ];

      $properties[self::PROPERTY_PATH] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function addFieldValues(ItemInterface $item) {
    if ($item->getDatasourceId() === 'entity:node') {
      $companyId = $item->getOriginalObject()->getValue()->id();

      $hasUpcomingEvents = \Drupal::service('company.service')->hasEvents($companyId, TRUE);

      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($item->getFields(), NULL, self::PROPERTY_PATH);

      foreach ($fields as $field) {
        $field->addValue($hasUpcomingEvents);
      }
    }
  }

}

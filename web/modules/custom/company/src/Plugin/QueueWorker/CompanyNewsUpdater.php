<?php

namespace Drupal\company\Plugin\QueueWorker;

use Drupal\company\Service\NewsServiceInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a queue worker for updating the recent news of a company.
 *
 * @QueueWorker(
 *   id = "company_news_updater",
 *   title = @Translation("Company News Updater"),
 *   cron = {
 *    "time" = 30
 *   }
 * )
 */
class CompanyNewsUpdater extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected NodeStorageInterface $nodeStorage;

  /**
   * The news service.
   *
   * @var \Drupal\company\Service\NewsServiceInterface
   */
  protected NewsServiceInterface $newsService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    NodeStorageInterface $nodeStorage,
    NewsServiceInterface $newsService
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->nodeStorage = $nodeStorage;
    $this->newsService = $newsService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('node'),
      $container->get('company.news_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    if (empty($data->nid)) {
      return;
    }

    $company = $this->nodeStorage->load($data->nid);
    if (!($company instanceof NodeInterface)) {
      return;
    }

    $this->updateCompanyNews($company);
  }

  /**
   * Updates the news of the company.
   *
   * @param \Drupal\node\NodeInterface $company
   *   The company node.
   */
  protected function updateCompanyNews(NodeInterface $company): void {
    if ($company->bundle() !== 'company') {
      throw new \InvalidArgumentException($this->t('Expecting node of type: Company, got: %bundle instead.', [
        '%bundle' => $company->bundle(),
      ]));
    }

    $news = $this->newsService->getNews($company->getTitle(), 3);
    if (empty($news)) {
      return;
    }

    $this->clearCompanyNews($company);

    $company->set('field_recent_news', $this->buildNewsFieldValues($news));

    try {
      $company->save();
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('company')->error($e->getMessage());
    }
  }

  /**
   * Clears the recent news field of the company.
   *
   * @param \Drupal\node\NodeInterface $company
   *   The company node.
   */
  private function clearCompanyNews(NodeInterface $company): void {
    $company->set('field_recent_news', []);

    try {
      $company->save();
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('company')->error($e->getMessage());
    }
  }

  /**
   * Builds the field values of Links.
   *
   * @param array $news
   *   The news array.
   *
   * @return array
   *   The field values.
   */
  private function buildNewsFieldValues(array $news): array {
    $fieldValues = [];
    foreach ($news as $article) {
      $fieldValues[] = [
        'title' => $article['title'],
        'uri' => $article['url'],
        'options' => [
          'attributes' => [
            'target' => '_blank',
          ],
        ],
      ];
    }
    return $fieldValues;
  }

}

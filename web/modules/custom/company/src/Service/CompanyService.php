<?php

namespace Drupal\company\Service;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\SearchApiException;

/**
 * Provides helper functions for working with events.
 */
class CompanyService {

  /**
   * Returns whether the specified company has (optional: upcoming) events.
   */
  public function hasEvents($companyId, $upcoming = FALSE) {
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'event');
    $query->condition('field_organizer', $companyId);

    if ($upcoming) {
      $now = new DrupalDateTime('now');
      $now = $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

      $query->condition('field_start_date', $now, '>=');
    }

    $eventIds = $query->execute();

    $events = Node::loadMultiple($eventIds);

    return (count($events) > 0);
  }

  /**
   * Force reindex companies index.
   */
  public function reindexCompanies() {
    $companiesIndex = Index::load('content');

    try {
      $companiesIndex->reindex();

      $companiesIndex->indexItems();
    }
    catch (SearchApiException $exception) {
      \Drupal::logger('CompanyService::reindexCompanies')->error($exception);
    }
  }

}

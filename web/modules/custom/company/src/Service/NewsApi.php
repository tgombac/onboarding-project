<?php

namespace Drupal\company\Service;

use Dotenv\Dotenv;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a way to retrieve news from the News API.
 */
class NewsApi implements NewsServiceInterface, ContainerInjectionInterface {

  const BASE_REQUEST_URI = 'https://newsapi.org/v2/everything';

  /**
   * The News API key.
   *
   * @var string|mixed
   */
  private string $apiKey;

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * Construct the NewsApi object.
   */
  public function __construct(ClientInterface $httpClient) {
    $this->httpClient = $httpClient;

    $dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
    $dotenv->load();
    if (isset($_ENV['NEWS_API_KEY'])) {
      $this->apiKey = $_ENV['NEWS_API_KEY'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getNews(string $query, ?int $limit = NULL): array {
    $resultsArray = $this->fetchData($query, $limit);
    if ($resultsArray === NULL || empty($resultsArray['totalResults'])) {
      return [];
    }

    return array_map(function ($article) {
      return [
        'title' => $article['title'],
        'description' => $article['description'],
        'url' => $article['url'],
        'image' => $article['urlToImage'],
        'publishedAt' => $article['publishedAt'],
      ];
    }, $resultsArray['articles']);
  }

  /**
   * Fetch the news data from the api.
   *
   * @param string $query
   *   The query string.
   * @param int|null $limit
   *   The amount of results to fetch.
   *
   * @return array|null
   *   The api results or NULL if failed.
   */
  protected function fetchData(string $query, ?int $limit): array|NULL {
    try {
      $response = $this->httpClient->request('GET', self::BASE_REQUEST_URI,
        $this->getRequestParameters($query, $limit));
    }
    catch (GuzzleException $e) {
      \Drupal::logger('company')->error($e->getMessage());

      return NULL;
    }

    if ($response->getStatusCode() !== 200) {
      return NULL;
    }

    $responseBody = $response->getBody()->getContents();
    return json_decode($responseBody, TRUE);
  }

  /**
   * Builds the request parameters array.
   *
   * @param string $query
   *   The query.
   * @param int|null $limit
   *   The amount of results to fetch.
   *
   * @return array
   *   The parameters array.
   */
  protected function getRequestParameters(string $query, ?int $limit): array {
    $parameters = [
      'query' => [
        'q' => $query,
        'sortBy' => 'publishedAt',
        'apiKey' => $this->apiKey,
      ],
    ];

    if ($limit !== NULL && $limit > 0 && $limit <= 100) {
      $parameters['query']['pageSize'] = (string) $limit;
    }

    return $parameters;
  }

}

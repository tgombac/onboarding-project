<?php

namespace Drupal\company\Service;

/**
 * Provides the News Service Interface.
 */
interface NewsServiceInterface {

  /**
   * Get the news for the specified query.
   *
   * @param string $query
   *   The query string.
   * @param int|null $limit
   *   The max number of results.
   *
   * @return array
   *   The array of results.
   */
  public function getNews(string $query, ?int $limit = NULL): array;

}

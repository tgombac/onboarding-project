<?php

namespace Drupal\company\Commands;

use Drush\Commands\DrushCommands;

/**
 * Provides drush commands related to the Company content type.
 */
class CompanyCommands extends DrushCommands {

  /**
   * Reindex companies search index.
   *
   * @command company:reindex
   */
  public function reindexCompanies() {
    \Drupal::service('company.service')->reindexCompanies();

    $this->output()->writeln('Reindexed companies!');
  }

}

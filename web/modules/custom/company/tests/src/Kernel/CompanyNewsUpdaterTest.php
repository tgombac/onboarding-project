<?php

namespace Drupal\Tests\company\Functional;

use Drupal\company\Plugin\QueueWorker\CompanyNewsUpdater;
use Drupal\company\Service\NewsServiceInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;

/**
 * Kernel testing of company news updater.
 */
class CompanyNewsUpdaterTest extends KernelTestBase {

  use EntityReferenceTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'text',
    'menu_ui',
    'node',
    'link',
    'company',
  ];

  /**
   * The news api mock service.
   *
   * @var \Drupal\company\Service\NewsServiceInterface
   */
  protected NewsServiceInterface $newsApiMock;

  /**
   * The company news updater queue worker.
   *
   * @var \Drupal\company\Plugin\QueueWorker\CompanyNewsUpdater
   */
  protected CompanyNewsUpdater $companyNewsUpdater;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->newsApiMock = $this->getMockBuilder('Drupal\company\Service\NewsApi')
      ->disableOriginalConstructor()
      ->getMock();
    $this->newsApiMock->expects($this->any())
      ->method('getNews')
      ->willReturn([
        [
          'title' => 'Lorem ipsum',
          'description' => 'dolor sit',
          'url' => 'amet,',
          'image' => 'consectetur adipiscing',
          'publishedAt' => 'elit.',
        ],
        [
          'title' => 'Donec feugiat varius',
          'description' => 'lorem,',
          'url' => ' a convallis',
          'image' => 'lectus molestie',
          'publishedAt' => 'ac.',
        ],
        [
          'title' => 'In maximus urna',
          'description' => 'nunc,',
          'url' => 'id suscipit',
          'image' => 'felis hendrerit',
          'publishedAt' => 'et.',
        ],
      ]);
    $this->container->set('company.news_api', $this->newsApiMock);

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');

    $this->installSchema('node', ['node_access']);

    $this->installConfig([
      'node',
      'company',
    ]);

    $this->companyNewsUpdater = $this->container->get('plugin.manager.queue_worker')->createInstance('company_news_updater');
  }

  /**
   * Tests that the company's news get updated upon item processing.
   */
  public function testCompanyNewsUpdate(): void {
    $company = Node::create([
      'type' => 'company',
      'title' => 'Hello World',
      'field_recent_news' => [],
    ]);
    $company->save();

    $recentNews = $company->field_recent_news->getValue();

    $this->assertEquals([], $recentNews);

    $this->companyNewsUpdater->processItem((object) ['nid' => $company->id()]);

    // Reload company from DB (expecting new values).
    $company = Node::load($company->id());
    $recentNews = $company->field_recent_news->getValue();

    $this->assertNotEmpty($recentNews);
    $this->assertEquals(3, count($recentNews));
  }

}

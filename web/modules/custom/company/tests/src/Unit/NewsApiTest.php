<?php

namespace Drupal\Tests\company\Unit;

use Drupal\company\Service\NewsApi;
use Drupal\company\Service\NewsServiceInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Unit tests for the NewsApi service.
 */
class NewsApiTest extends UnitTestCase {

  /**
   * The response headers.
   *
   * @var array|string[]
   */
  private static array $headers = ['Content-Type' => 'application/json'];

  /**
   * The response body.
   *
   * @var array
   */
  private array $responseBody = [
    'totalResults' => 3,
    'articles' => [
      [
        'title' => 'Lorem ipsum',
        'description' => 'dolor sit',
        'url' => 'amet,',
        'urlToImage' => 'consectetur adipiscing',
        'publishedAt' => 'elit.',
      ],
      [
        'title' => 'Donec feugiat varius',
        'description' => 'lorem,',
        'url' => ' a convallis',
        'urlToImage' => 'lectus molestie',
        'publishedAt' => 'ac.',
      ],
      [
        'title' => 'In maximus urna',
        'description' => 'nunc,',
        'url' => 'id suscipit',
        'urlToImage' => 'felis hendrerit',
        'publishedAt' => 'et.',
      ],
    ],
  ];

  /**
   * NewsApi instance.
   *
   * @var \Drupal\company\Service\NewsServiceInterface
   */
  protected NewsServiceInterface $newsApi;

  /**
   * The mock handler.
   *
   * @var \GuzzleHttp\Handler\MockHandler
   */
  protected MockHandler $mockHandler;

  /**
   * The mock client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $mockClient;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->mockHandler = new MockHandler();
    $handler = HandlerStack::create($this->mockHandler);
    $this->mockClient = new Client(['handler' => $handler]);

    $this->newsApi = new NewsApi($this->mockClient);

    $this->mockLogger();
  }

  /**
   * Mocks the logger service.
   */
  private function mockLogger(): void {
    $mockDBLog = $this->getMockBuilder('Drupal\dblog\Logger\DbLog')
      ->disableOriginalConstructor()
      ->getMock();

    $mockLoggerFactory = $this->getMockBuilder('Drupal\Core\Logger\LoggerChannelFactory')
      ->disableOriginalConstructor()
      ->getMock();
    $mockLoggerFactory->expects($this->any())
      ->method('get')
      ->willReturn($mockDBLog);

    $container = new ContainerBuilder();
    $container->set('logger.factory', $mockLoggerFactory);

    \Drupal::setContainer($container);
  }

  /**
   * Tests that the get news method returns the expected values.
   */
  public function testGetNewsSuccessful(): void {
    $this->mockHandler->append(new Response(200, self::$headers, json_encode($this->responseBody)));

    $news = $this->newsApi->getNews('Apple', 3);

    $this->assertEquals($this->responseBody['articles'][0]['title'], $news[0]['title']);
    $this->assertEquals($this->responseBody['articles'][1]['url'], $news[1]['url']);
    $this->assertEquals($this->responseBody['articles'][2]['urlToImage'], $news[2]['image']);
  }

  /**
   * Tests that get news method returns empty array on a non 200 response code.
   */
  public function testGetNewsUnsuccessful() {
    $this->mockHandler->append(new Response(403, self::$headers, json_encode($this->responseBody)));

    $news = $this->newsApi->getNews('Apple', 3);
    $this->assertEquals([], $news);
  }

  /**
   * Tests that the get news method returns an empty array when no results.
   */
  public function testGetNewsReturnsEmptyArray() {
    $responseBody = $this->responseBody;
    $responseBody['articles'] = [];
    $responseBody['totalResults'] = 0;

    $this->mockHandler->append(new Response(200, self::$headers, json_encode($responseBody)));

    $news = $this->newsApi->getNews('Apple', 3);
    $this->assertEquals([], $news);
  }

  /**
   * Cleanup.
   */
  public function tearDown(): void {
    unset($this->newsApi);
  }

}

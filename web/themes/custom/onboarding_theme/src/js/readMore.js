(function ($, Drupal, once) {
  const tabletBreakpoint = 768;

  const readMoreElements = () => {
    return $('.js--read-more');
  }

  const toggleButtons = () => {
    return $('.js--toggle-button');
  }

  const getTogglableElements = () => {
    return readMoreElements().find('p').slice(1);
  }

  const toggle = () => {
    getTogglableElements().toggle();
    toggleButtons().find('span').toggle();
  }

  const attachReadMoreFunctionality = () => {
    if (toggleButtons().length > 0) {
      return;
    }

    const readMoreNode = document.createElement('span');
    readMoreNode.appendChild(document.createTextNode(Drupal.t('Read more')));

    const showLessNode = document.createElement('span');
    showLessNode.appendChild(document.createTextNode(Drupal.t('Show less')));
    showLessNode.style.display = 'none';

    const toggleButton = document.createElement('a');
    toggleButton.appendChild(readMoreNode);
    toggleButton.appendChild(showLessNode);
    toggleButton.href = '#';
    toggleButton.classList.add('js--toggle-button');

    $(toggleButton).click(toggle);

    getTogglableElements().hide();

    readMoreElements().append(toggleButton);
  }

  const detachReadMoreFunctionality = () => {
    if (toggleButtons().length === 0) {
      return;
    }

    getTogglableElements().show();
    toggleButtons().remove();
  }

  const onResize = () => {
    if ($(window).width() < tabletBreakpoint) {
      return attachReadMoreFunctionality();
    }

    return detachReadMoreFunctionality();
  }

  const init = () => {
    onResize();

    $(window).resize(onResize)
  }


  Drupal.behaviors.readMore = {
    attach: (context) => {
      $(once('read-more', 'body', context)).each(init);
    }
  }
}(jQuery, Drupal, once));

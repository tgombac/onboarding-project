(function ($, Drupal, once) {
  const generateButton = (text) => {
    const button = document.createElement('button');
    button.innerHTML = text;

    return button;
  }

  const customSlider = (settings, context) => {
    $(once('custom-slider', settings.parent, context)).each(function () {
      const slideSelector = `${settings.parent} ${settings.target}`;

      const previous = generateButton('Prev.');
      const next = generateButton('Next');

      $(previous).on('click', () => {
        $(settings.parent, context).trigger('custom-slider-previous');
      });

      $(next).on('click', () => {
        $(settings.parent, context).trigger('custom-slider-next');
      });

      $(this).append(previous);
      $(this).append(next);

      const lastSlide = $(slideSelector, context).last();
      const lastSlideClone = lastSlide.clone();
      lastSlide.parent().prepend(lastSlideClone);
      $(slideSelector, context).each(function () {
        $(this).css('transition', 'none');
        $(this).css('transform', 'translate(-100%, 0)');
      });

      $(this).on('custom-slider-next', () => {
        $(slideSelector, context).each(function (index) {
          $(this).css('transition', '');
          if (index === 0 || index > 2) {
            return;
          }
          $(this).css('transform', 'translate(-200%, 0)');

          if (index === 1) {
            setTimeout(function () {
              const firstSlide = arguments[0];
              const secondSlide = firstSlide.next();
              const parent = firstSlide.parent();

              secondSlide.css('transition', 'none');
              secondSlide.css('transform', 'translate(-100%, 0)');
              secondSlide.css('transition', 'none');

              firstSlide.css('transition', 'none');
              firstSlide.css('transform', '');
              $(parent).append(firstSlide);
              firstSlide.css('transition', '');
            }, settings.transitionDuration, $(this));
          }
        });
      });

      $(this).on('custom-slider-previous', () => {
        $(slideSelector, context).each(function (index) {
          $(this).css('transition', '');
          if (index > 1) {
            return;
          }
          $(this).css('transform', 'translate(0, 0)');
        });

        setTimeout(function () {
          const lastSlide = $(slideSelector, context).last();
          lastSlide.parent().prepend(lastSlide);
          $(slideSelector, context).each(function () {
            $(this).css('transition', 'none');
            $(this).css('transform', 'translate(-100%, 0)');
          });
        }, settings.transitionDuration);
      });
    });
  }

  Drupal.behaviors.customSlider = {
    attach: (context) => {
      customSlider({
        'parent': '.field--name-field-view .view-slider-view',
        'target': '.view-content >.views-row',
        'transitionDuration': 2000,
      }, context);
    }
  }
}(jQuery, Drupal, once));

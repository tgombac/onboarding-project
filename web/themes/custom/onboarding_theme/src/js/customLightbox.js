(function ($, Drupal, once) {
  Drupal.behaviors.customLightbox = {
    attach: (context) => {
      $(once(
          'custom-lightbox',
          '.page-node-type-event .field--name-field-images',
          context
      )).each(function () {
        const lightboxContainer = document.createElement('div');
        lightboxContainer.classList.add('custom-lightbox');
        const lightboxImage = document.createElement('div');
        lightboxImage.classList.add('lightbox-image');
        const lightboxList = document.createElement('div');
        lightboxList.classList.add('lightbox-list');
        const closeButton = document.createElement('button');
        closeButton.classList.add('lightbox-close-button');
        closeButton.innerHTML = 'Close';
        $(closeButton).on('click', () => {
          $(lightboxContainer).css('display', 'none');
        });

        $(document).keyup(function (e) {
          if (e.key === 'Escape') {
            $(lightboxContainer).css('display', 'none');
          }
        });

        $('.field__item', this).each(function (index) {
          $(this).on('click', () => {
            $(lightboxContainer).css('display', 'block');
          });

          const clone = $(this).clone();
          if (index === 0) {
            const imageClone = $('img', this).clone();
            lightboxImage.append(imageClone[0]);
          }
          clone.on('click', () => {
            $('img', lightboxImage).remove();
            const currentImageClone = $('img', clone).clone();
            lightboxImage.append(currentImageClone[0]);
          });
          lightboxList.append(clone[0]);
        });

        lightboxContainer.append(closeButton);
        lightboxContainer.append(lightboxImage);
        lightboxContainer.append(lightboxList);

        $('.layout-container').append($(lightboxContainer));
      });
    }
  }
}(jQuery, Drupal, once));

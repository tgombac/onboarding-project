(function ($, Drupal, once) {
  const setup = (viewRows, imageDisplayArea) => {
    viewRows.each(function (index) {
      const imageElement = $('.views-field.views-field-field-image img', this);
      const imageUrl = imageElement.attr('src');

      const generatedImageElement = generateImageElement(imageUrl, index, imageElement);
      imageDisplayArea.append(generatedImageElement);

      // Shuffle images on hover of view event button.
      $('.views-field.views-field-nothing .field-content', this).on('mouseenter', function () {
        $('img', imageDisplayArea).css('opacity', '0');
        $(`img[data-index=${index}]`, imageDisplayArea).css('opacity', '1');
      });
    });
  }

  const generateImageElement = (imageUrl, index, imageSourceElement) => {
    const imageElement = document.createElement('img');
    imageElement.src = imageUrl;
    imageElement.alt = imageSourceElement.attr('alt');
    imageElement.dataset.index = index;

    if (index !== 0) {
      imageElement.style.opacity = '0';
    }

    return imageElement;
  }

  const generateImageDisplayArea = (viewElement) => {
    const imageDisplayArea = document.createElement('div');
    imageDisplayArea.classList.add('image-display-area');
    viewElement.append(imageDisplayArea);
  }

  Drupal.behaviors.upcomingEvents = {
    attach: (context) => {
      $(once('upcoming-events', '.view.view-upcoming-events', context)).each(function () {
        generateImageDisplayArea($(this));

        setup($('.views-row', this), $('.image-display-area', this));
      });
    }
  }
}(jQuery, Drupal, once));

(function ($, Drupal, once) {
  Drupal.behaviors.animateEventsLoad = {
    attach: (context) => {
      console.log('Called');
      $(once('event-animation', 'body.path-events .view-events-list .views-row', context)).each(function () {
        $(this).addClass('animate-event');
      });
    }
  }
}(jQuery, Drupal, once));

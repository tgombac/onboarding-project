(function ($, Drupal, once) {
  Drupal.behaviors.accordion = {
    attach: (context) => {
      $(once('accordion', '.paragraph--type--question-answer', context)).each(function () {
        const element = $(this);

        $('.field.field--name-field-question', element).on('click', function () {
          $('.field.field--name-field-answer', element).toggle();

          $(this).toggleClass('expanded');
        });
      });
    }
  }
}(jQuery, Drupal, once));

(function ($, Drupal, once) {
  Drupal.behaviors.headerMobileBackground = {
    attach: (context) => {
      $(once(
          'header-mobile-background',
          'header.header',
          context
      )).each(() => {
        console.log(navigator.userAgent);
        if (navigator.userAgent.match(/Android/i) ||
            navigator.userAgent.match(/webOS/i) ||
            navigator.userAgent.match(/iPhone/i) ||
            navigator.userAgent.match(/iPod/i)
        ) {
          $('header.header').css('background-color', 'rgba(0, 0, 0, 0.2');
        }
      })
    }
  }
}(jQuery, Drupal, once));

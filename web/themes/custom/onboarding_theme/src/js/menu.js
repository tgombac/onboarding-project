(function ($, Drupal, once) {
  Drupal.behaviors.stickyMenuButton = {
    attach: (context) => {
      $(once(
          'sticky-menu-icon',
          '#menu-icon',
          context
      )).each(() => {
        const menuIcon = $('#menu-icon');
        menuIcon.on('click', (event) => {
          event.preventDefault();

          const menu = $('section.menu');
          if (menu.hasClass('active')) {
            menu.removeClass('active');
            menuIcon.removeClass('active');

            $('body').css('overflow', 'auto');
          }
          else {
            menu.addClass('active');
            menuIcon.addClass('active');

            $('body').css('overflow', 'hidden');
          }
        });

        const headerOffsetTop = $('header.header').offset().top;
        const layoutContainerOffsetTop = $('.layout-container').offset().top;

        // Could and probably should be solved with CSS-only?
        $(window).scroll(() => {
          let scrollTop = $(window).scrollTop();

          // If the layout container offset top is greater than 0,
          //  it means that the admin toolbar is open, so we should
          //  take that into account IF the toolbar isn't fixed.
          if (layoutContainerOffsetTop > 0 && $('body.toolbar-fixed').length === 1) {
            scrollTop += layoutContainerOffsetTop;
          }

          if (scrollTop >= headerOffsetTop) {
            menuIcon.addClass('fixed');
          }
          else {
            menuIcon.removeClass('fixed');
          }
        })
      })
    }
  }
}(jQuery, Drupal, once));

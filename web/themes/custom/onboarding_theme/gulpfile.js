'use strict';

const gulp         = require('gulp');
const sass         = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS     = require('gulp-clean-css');
const uglify       = require('gulp-uglify');
const concat       = require('gulp-concat');
const sourcemaps   = require('gulp-sourcemaps');
const del          = require('del');
const stylelint    = require('gulp-stylelint');
const eslint       = require('gulp-eslint');

const STYLES_SRC            = './src/scss/styles.scss';
const STYLES_SRC_PATTERN    = './src/scss/**/*.scss';
const STYLES_DEST           = './build/css/';
const SCRIPTS_SRC           = './src/js/**/*.js';
const SCRIPTS_DEST_FILENAME = 'script.js';
const SCRIPTS_DEST          = './build/js/';
const JS_CLEAN_PATTERN      = 'build/js/*';
const CSS_CLEAN_PATTERN     = './build/css/*';

/**
 * Clean Js files (and source maps).
 */
function cleanJS() {
  return del([JS_CLEAN_PATTERN]);
}

/**
 * Clean CSS files (and source maps).
 */
function cleanCss() {
  return del([CSS_CLEAN_PATTERN]);
}

/**
 * Build scss for production.
 */
function buildStyles() {
  return gulp.src(STYLES_SRC)
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(cleanCSS())
      .pipe(gulp.dest(STYLES_DEST));
}

/**
 * Build scss for development.
 */
function buildStylesDev() {
  return gulp.src(STYLES_SRC)
      .pipe(sourcemaps.init())
      .pipe(stylelint({
        failAfterError: false,
        reporters: [
          {
            formatter: 'string', console: true,
          },
        ],
      }))
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(STYLES_DEST));
}

/**
 * Build js for production.
 */
function buildScripts() {
  return gulp.src(SCRIPTS_SRC)
      .pipe(concat(SCRIPTS_DEST_FILENAME))
      .pipe(uglify())
      .pipe(gulp.dest(SCRIPTS_DEST));
}

/**
 * Build js for development.
 */
function buildScriptsDev() {
  return gulp.src(SCRIPTS_SRC)
      .pipe(eslint())
      .pipe(eslint.format('stylish', console.log))
      .pipe(sourcemaps.init())
      .pipe(concat(SCRIPTS_DEST_FILENAME))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(SCRIPTS_DEST));
}

/**
 * Watch files.
 */
function watchFiles() {
  gulp.watch(STYLES_SRC_PATTERN, gulp.series(buildStylesDev));
  gulp.watch(SCRIPTS_SRC, gulp.series(buildScriptsDev));
}

gulp.task('clean', gulp.series(cleanJS, cleanCss));

gulp.task('prod', gulp.series('clean', gulp.parallel(buildStyles, buildScripts)));
gulp.task('dev', gulp.series('clean', gulp.parallel(buildStylesDev, buildScriptsDev)));

gulp.task('watch', gulp.series('dev', watchFiles));

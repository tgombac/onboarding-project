const path = require('path');
const fs   = require('fs');

const filenames = fs.readdirSync(path.join(__dirname, 'src/js'))
    .map(filename => path.join(__dirname, 'src/js', filename));

module.exports = {
  entry: filenames,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};

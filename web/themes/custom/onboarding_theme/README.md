# Onboarding theme
Awesome onboarding theme.

## Project structure
The project was built using Node version `18.12.1` with Node Package Manager (npm) version `9.1.1`.

The project source files reside inside the `src/` directory:

- The `src/scss/` directory stores the `*.scss` files, with the `styles.scss` file being the main entrypoint for the
build tasks.
- The `src/js/` directory stores the `*.js` files, with the `./src/js/**/*.js` pattern being used for the build tasks.
- The built files resides inside the `build` directory. The `styles.css` and `styles.css.map` files reside in the
`build/css/` directory, and the `script.js` and `script.js.map` files reside in the `build/js/` directory.

## Setup
Run `npm install`.

## Package.json scripts

- To build the styles and scripts for production, run `npm run prod`. This will run `gulp prod` in the background.
- To build the styles and scripts for development, run `npm run dev`. This will run `gulp dev` in the background.
- To build the styles and scripts for development automatically on file changes, run `npm run watch`.
This will run `gulp watch` in the background.
- To clean up the `build` folder, run `npm run clean`. This will run `gulp clean` in the background.

## Gulp tasks
There are 4 gulp tasks available:

- `clean`: Deletes all the files in the `build/css/` and `build/js/` folders.
- `prod`: Runs the gulp `clean` task first. Then, all the `*.js` files in the `src/js/` directory are concatenated and
uglified, and the output is written into the `build/js/script.js` file. The `src/styles.scss` is compiled into css,
autoprefixer is run, the css is then optimized
[See clean-css Level 1 optimizations](https://github.com/clean-css/clean-css#level-1-optimizations) and the output is
then written to the `build/js/styles.css` file.
- `dev`: Runs the gulp `clean` task first. Then, all the `*.js` files in `src/js/` are concatenated, sourcemaps are
generated, and the output is written into the `build/js/` directory. The `src/styles.scss` is compiled into css,
autoprefixer is run, sourcemaps are generated, and the output is written into the `build/css/` directory.
This task will also lint all the scss and js files (see `.eslintrc` and `.stylelintrc` for more information on their
configuration).
- `watch`: Runs the gulp `dev` task first, then watches the `./src/js/**/*.js` and `./src/scss/**/*.scss` patterns for
any file changes and builds the files accordingly.

<?php

/**
 * Implements hook_preprocess_HOOK().
 */
function onboarding_theme_preprocess_select(&$variables) {
  if (isset($variables['element']['#title'])) {
    $variables['options'][0]['label'] = t('All');
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function onboarding_theme_preprocess_views_view_fields(&$variables) {
  try {
    $variables['entity_url'] = $variables['row']->_entity->toUrl();
  } catch (\Exception $e) {
    \Drupal::logger('onboarding_theme')->alert($e->getMessage());
  }
}

/**
 * Implements template_preprocess_paragraph().
 */
function onboarding_theme_preprocess_paragraph__pt_text_with_image(&$variables) {
  $imageAlignmentField = $variables['paragraph']->get('field_image_alignment');
  $allowedValues = $imageAlignmentField->getSetting('allowed_values');
  if ($allowedValues[$imageAlignmentField->value] === 'right') {
    $variables['attributes']['class'] = ['align-image-right'];
  }
}

/**
 * Implements template_preprocess_paragraph().
 */
function onboarding_theme_preprocess_paragraph__quote(&$variables) {
  if (isset($variables['content']['field_image']['#items'])) {
    return;
  }
  $variables['attributes']['class'] = ['without-image'];
}

/**
 * Implements template_preprocess_paragraph().
 */
function onboarding_theme_preprocess_paragraph__view_embed(&$variables) {
  if ($variables['elements']['#paragraph']->get('field_view')[0]->target_id !== 'most_visited_latest_events') {
    return;
  }

  $variables['attributes']['class'][] = 'most-visited-latest-events';
}

/**
 * Implements template_preprocess_paragraph().
 */
function onboarding_theme_preprocess_paragraph__pt_slide(&$variables) {
  $variables['attributes']['style'][] = 'background: ' . $variables['paragraph']->field_background_color->value . ';';
}

/**
 * Implements hook_preprocess_HOOK().
 */
function onboarding_theme_preprocess_page(&$variables) {
  if (!isset($variables['node']) || $variables['node']->type->target_id !== 'page') {
    return;
  }

  $node = $variables['node'];
  if (count($node->get('field_components')) === 0) {
    return;
  }

  $slider = NULL;
  foreach ($node->get('field_components') as $index => $component) {
    if ($component->entity->get('type')->target_id === 'pt_slider') {
      $slider = $component;
      break;
    }
  }
  if ($slider === NULL) {
    return;
  }

  $variables['slider_target_id'] = $slider->target_id;
}

/**
 * Implements template_preprocess_field().
 */
function onboarding_theme_preprocess_field(&$variables) {
  $fieldName = $variables['element']['#field_name'];
  if ($fieldName === 'field_components') {
    foreach ($variables['items'] as $index => $item) {
      if ($item['content']['#paragraph']->get('type')->target_id === 'pt_slider') {
        unset($variables['items'][$index]);
      }
    }
  }
  elseif ($fieldName === 'field_event_type') {
    $eventTypeTerm = $variables['element'][0]['#options']['entity'];
    $color = $eventTypeTerm->get('field_color')->color;
    if ($color === NULL) {
      return;
    }
    $variables['event_type_color'] = $color;
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function onboarding_theme_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  $node = \Drupal::routeMatch()->getParameter('node');
  if (empty($node) || $node->bundle() !== 'location') {
    return;
  }

  $isOperating = (bool) $node->get('field_is_operating')->value;
  if ($isOperating) {
    return;
  }

  $suggestions[] = 'node__location__not_operating';
}
